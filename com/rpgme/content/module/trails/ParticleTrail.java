package com.rpgme.content.module.trails;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.math.Vec3D;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

/**
 * Created by Robin on 08/02/2017.
 */
public class ParticleTrail {

    private static IntMap<ParticleTrail> trailMap = new IntMap<>(20);
    static {
        trailMap.put(SkillType.ATTACK, new ParticleTrail(198, 0, 0, 72, 0, 0));
        trailMap.put(SkillType.DEFENCE, new ParticleTrail(51, 102, 255, 229, 255, 255));
        trailMap.put(SkillType.ARCHERY, new ParticleTrail(50, 205, 50, 255, 160, 20));
        trailMap.put(SkillType.MINING, new ParticleTrail(58, 58, 75, 0, 164, 164));
        trailMap.put(SkillType.LANDSCAPING, new ParticleTrail(240, 240, 140, 92, 120, 35));
        trailMap.put(SkillType.WOODCUTTING, new ParticleTrail(102, 204, 0, 160, 84, 32));
        trailMap.put(SkillType.FISHING, new ParticleTrail(30, 144, 255, 64, 224, 208));
        trailMap.put(SkillType.FARMING, new ParticleTrail(30, 144, 255, 64, 224, 208));
        trailMap.put(SkillType.FORGING, new ParticleTrail(42, 42, 42, 255, 214, 10));
        trailMap.put(SkillType.ALCHEMY, new ParticleTrail(142, 0, 198, 128, 204, 0));
        trailMap.put(SkillType.ENCHANTING, new ParticleTrail(20, 138, 255, 153, 0, 204));
        trailMap.put(SkillType.STAMINA, new ParticleTrail(176, 196, 222, 245, 245, 245));
        trailMap.put(SkillType.TAMING, new ParticleTrail(224, 112, 0, 255, 218, 185));
    }

    public static void setTrailColor(int skillId, ParticleTrail trail) {
        trailMap.put(skillId, trail);
    }

    public static ParticleTrail valueOf(int skillId) {
        return trailMap.get(skillId);
    }

    public static ParticleTrail valueOf(Skill skill) {
        return trailMap.get(skill.getId());
    }

    public static ParticleTrail valueOf(String name) {
        Skill skill = RPGme.getInstance().getSkill(name);
        return skill == null ? null : trailMap.get(skill.getId());
    }
//    ATTACK(198, 0, 0, 72, 0, 0),
//    DEFENCE(51, 102, 255, 229, 255, 255),
//    ARCHERY(50, 205, 50, 255, 160, 20),
//    MINING(58, 58, 75, 0, 164, 164),
//    LANDSCAPING(240, 240, 140, 92, 120, 35),
//    WOODCUTTING(102, 204, 0, 160, 84, 32),
//    FISHING(30, 144, 255, 64, 224, 208),
//    FARMING(5, 130, 5, 215, 15, 15),
//    FORGING(42, 42, 42, 255, 214, 10),
//    ALCHEMY(142, 0, 198, 128, 204, 0),
//    ENCHANTING(20, 138, 255, 153, 0, 204),
//    STAMINA(176, 196, 222, 245, 245, 245),
//    TAMING(224, 112, 0, 255, 218, 185),
//    SUMMONING(255, 0, 202, 150, 21, 95);

    private final boolean bicolor;
    private final Vec3D[] colors;

    private ParticleTrail(int r, int g, int b) {
        this.bicolor = false;
        this.colors = new Vec3D[]{
            new Vec3D(r / 255.0, g / 255.0, b / 255.0)
        };
    }

    private ParticleTrail(int r, int g, int b, int r2, int g2, int b2) {
        this.bicolor = true;
        this.colors = new Vec3D[]{
            new Vec3D(r / 255.0, g / 255.0, b / 255.0),
            new Vec3D(r2 / 255.0, g2 / 255.0, b2 / 255.0)
        };
    }

    public void display(Player p) {
        Location loc = p.getLocation().add(0.0D, 0.2D, 0.0D);
        int i;
        if(this.bicolor) {
            for(i = 0; i < 2; ++i) {
                loc.getWorld().spawnParticle(Particle.SPELL_MOB, loc, 0, colors[0].x, colors[0].y, colors[0].z, 1.0);
                loc.getWorld().spawnParticle(Particle.SPELL_MOB, loc, 0, colors[1].x, colors[1].y, colors[1].z, 1.0);
            }
        } else {
            for(i = 0; i < 4; ++i) {
                loc.getWorld().spawnParticle(Particle.SPELL_MOB, loc, 0, colors[0].x, colors[0].y, colors[0].z, 1.0);
            }
        }
    }

    public int getSkillId() {
        for(IntMap.Entry<ParticleTrail> values : trailMap.entries()) {
            if(values.value == this) {
                return values.key;
            }
        }
        return -1;
    }

    public Skill getSkill() {
        int id = getSkillId();
        if(id > 0) {
            return RPGme.getInstance().getSkill(id);
        }
        return null;
    }

    public String getName() {
        Skill skill = getSkill();
        if(skill != null) {
            return skill.getName();
        }
        return "";
    }
}