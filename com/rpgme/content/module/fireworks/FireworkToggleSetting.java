package com.rpgme.content.module.fireworks;

import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.effect.EnchantmentGlow;
import com.rpgme.plugin.util.menu.Settings;
import org.bukkit.Material;

/**
 * Created by Robin on 26/02/2017.
 */
public class FireworkToggleSetting extends Settings.Setting {

    public static final String KEY = "fireworks";
    public static final String ON = "ON";
    public static final String OFF = "OFF";

    public FireworkToggleSetting(RPGPlayer player) {
        super(player, "fireworks", 0);
    }

    @Override
    public void putItems() {
        this.put("ON", EnchantmentGlow.addGlow(ItemUtils.create(Material.PAPER, "&6Firework Shows", "Launch a show of fireworks when you reach level milestones.")));
        this.put("OFF", ItemUtils.create(Material.EMPTY_MAP, "&6Firework Shows", "&7Do &lnot &7launch firework shows."));
        Settings.addToggleMenu(this.items);
    }
}
