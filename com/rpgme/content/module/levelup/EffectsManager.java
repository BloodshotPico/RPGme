package com.rpgme.content.module.levelup;

import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.effect.EnchantmentGlow;
import com.rpgme.plugin.util.menu.ConfigurationMenu;
import com.rpgme.plugin.util.menu.Settings;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 05/02/2017.
 */
public class EffectsManager implements ListenerModule {

    public static final String SETTING_KEY = "LevelupEffect";
    private static final String PERMISSION_WILDCARD = "rpgme.particles.*";

    private static final String SETTING_TITLE = "&5Levelup Particle Animation";
    private static final String[] effects = new String[]{"Default", "Flame Helix", "Beacon", "Eruption", "Pulsar"};

    static final List<LivingEntity> currentTasks = new ArrayList<>();

    private static boolean unlockAll = true;

    public static void startByName(Player target, String name) {
            if(!currentTasks.contains(target)) {
                currentTasks.add(target);

                switch(name) {
                    case "ERUPTION":
                        new LevelupEffect.Eruption(target).start();
                        break;
                    case "PULSAR":
                        new LevelupEffect.Pulsar(target).start();
                        break;
                    case "FLAMEHELIX":
                        new LevelupEffect.FlameHelix(target).start();
                        break;
                    case "BEACON":
                        new LevelupEffect.Beacon(target).start();
                        break;
                    default:
                        new LevelupEffect.Default(target).start();
                        break;
                }

        }
    }

    @Override
    public String getName() {
        return "Levelup Effects";
    }

    @Override
    public void onEnable() {
        ConfigurationMenu.registerSetting(6, EffectSetting.class);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        config.addValue("If true, option will only be available for players with permission 'rpgme.particles.<effect>'",
                "require permissions", false);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        unlockAll = config.getBoolean("require permissions");
    }

    public static class EffectSetting extends Settings.Setting {

        public EffectSetting(RPGPlayer player) {
            super(player, SETTING_KEY, 0);
        }

        @Override
        public void putItems() {
            Player p = this.player.getPlayer();
            int count = effects.length;

            for(int i = 0; i < count; ++i) {
                String effect = effects[i];
                if(i == 0 || unlockAll || p.hasPermission(PERMISSION_WILDCARD) || p.hasPermission(effectPermission(effect))) {
                    super.put(effect.toUpperCase().replace(" ", ""), getAsItem(effect));
                }
            }

            if(!items.isEmpty()) {
                Settings.addToggleMenu(this.items);
            }

        }

        private String effectPermission(String name) {
            return "rpgme.particles." + name.toLowerCase().replace(" ", "");
        }

        private ItemStack getAsItem(String name) {
            String desc = name.equals("Default") ? "Use the default particles" : "Use particle effect &6" + name;
            return EnchantmentGlow.addGlow(ItemUtils.create(Material.PAPER, SETTING_TITLE, desc));
        }
    }
}
