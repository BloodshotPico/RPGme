package com.rpgme.content.module.expboost;

import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Listener;
import com.rpgme.plugin.api.ListenerModule;
import com.rpgme.plugin.event.RPGPlayerJoinEvent;
import com.rpgme.plugin.event.SkillExpGainEvent;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.YamlFile;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.util.*;

public class BoosterModule extends Listener<RPGme> implements ListenerModule {

    private static final String STORED_BOOST = "BoostRemaining";
    private static final String UNPAUSED_BOOST = "RunningBoost";

    private final BoosterCommand command;
    private List<Boost> globalBoosters;
    private Map<Player, Boost> boosterMap = new HashMap<>();

    private boolean pauseOnLogout;
    private boolean playSounds;

    public BoosterModule(RPGme plugin) {
        super(plugin);
        this.command = new BoosterCommand(plugin);
    }

    @Override
    public String getName() {
        return "ExpBoost";
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        config.addValue("Should active exp boosts be paused when a player logs out", "pause on logout", false)
                .addValue("play sounds", true);

        messages.addValue("global", "&fServer multiplier (by &6{0}&f):  &3")
                .addValue("personal", "&fPersonal multiplier:  &3")
                .addValue("nothing", "There are currently no active boosters.");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        pauseOnLogout = config.getBoolean("pause on logout", false);
        playSounds = config.getBoolean("play sounds", true);
    }

    public static String readableBoost(int percent) {
        return String.format(Locale.ENGLISH, "x%.2f", toMultiplier(percent));
    }

    public static double toMultiplier(int percent) {
        return percent / 100.0;
    }

    private long getEndtime(int mins) {
        return System.currentTimeMillis() + (long)(mins * 60 * 1000);
    }

    @Override
    public void onEnable() {
        command.register();
        YamlFile file = new YamlFile(new File(plugin.getUserDataFolder(), "boosters.temp"));

        if(file.getFile().exists()) {
            file.read();

            List<String> stored = file.data.getStringList("global");
            globalBoosters = new ArrayList<>(stored.size());

            for(String s : stored) {
                int index = s.indexOf('!');
                globalBoosters.add(Boost.deserializeStored(s.substring(index + 1)).setUsername(s.substring(0, index)));
            }

            file.getFile().delete();
        }
    }

    @Override
    public void onDisable() {
        if(globalBoosters != null && !globalBoosters.isEmpty()) {
            YamlFile file = new YamlFile(new File(plugin.getUserDataFolder(), "boosters.temp"));
            List<String> stored = new ArrayList<>();

            for(Boost b : globalBoosters) {
                stored.add(b.getUsername() + '!' + b.serializeStored());
            }

            file.getData().set("global", stored);
            file.write();
        }

        List<Player> keys = new ArrayList<>(boosterMap.keySet());
        for(Player p : keys) {
            remove(p);
        }
    }

    @EventHandler
    public void onExpGain(SkillExpGainEvent event) {
        Player player = event.getPlayer();
        int boost = 0;
        Boost global = getGlobalBoost();
        if(global != null) {
            boost += global.boost - 100;
        }

        Boost personal = getBoost(player);
        if(personal != null) {
            boost += personal.boost - 100;
        }

        if(boost > 0) {
            double multiplier = toMultiplier(100 + boost);
            event.setExp((float) (event.getExp() * multiplier));
            if(event.getSkillId() != SkillType.STAMINA && playSounds) {
                GameSound.play(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, player, 0.1F, 1.4F, 0.5D);
            }
        }

    }

    @EventHandler
    public void onLogin(RPGPlayerJoinEvent event) {
        RPGPlayer player = event.getRPGPlayer();
        String running = player.getSetting("RunningBoost");
        if(running != null) {
            Boost stored = Boost.deserializeRunning(running);
            if(stored != null) {
                stored.setUsername(event.getPlayer().getName());
                boosterMap.put(event.getPlayer(), stored);
            }

            player.setSetting("RunningBoost", null);
            player.getPlayer().sendMessage(command.buildBoosterOverview(player.getPlayer()));
        }

        String stored = player.getSetting("BoostRemaining");
        if(stored != null) {
            String[] boosts = stored.split(";");
            Boost primary = Boost.deserializeStored(boosts[0]);
            primary.setUsername(event.getPlayer().getName());

            boosterMap.put(event.getPlayer(), primary);
            if(boosts.length > 1) {
                String tostore = stored.substring(stored.indexOf(";") + 1);
                player.setSetting("BoostRemaining", tostore);
            } else {
                player.setSetting("BoostRemaining", null);
            }

            player.getPlayer().sendMessage(command.buildBoosterOverview(player.getPlayer()));
        } else if(getGlobalBoost() != null) {
            player.getPlayer().sendMessage(command.buildBoosterOverview(player.getPlayer()));
        }

    }

    public void remove(Player player) {
        Boost current = boosterMap.remove(player);
        if(current != null) {
            RPGPlayer rp = plugin.getPlayer(player);
            if(!pauseOnLogout) {
                rp.setSetting("RunningBoost", current.serializeRunning());
            } else {
                String stored = rp.getSetting("BoostRemaining");
                stored = current.serializeStored() + (stored != null ? ";" + stored : "");
                rp.setSetting("BoostRemaining", stored);
            }
        }

    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLeave(PlayerQuitEvent e) {
        remove(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onKick(PlayerKickEvent e) {
        remove(e.getPlayer());
    }

    public Boost getBoost(Player player) {
        Boost boost = boosterMap.get(player);
        
        if(boost != null && !boost.isValid()) {
            boosterMap.remove(player);
            onLogin(new RPGPlayerJoinEvent(plugin.getPlayer(player)));
            return getBoost(player);
        } else {
            return boost;
        }
    }

    public BoosterModule.Boost getGlobalBoost() {
        Boost global = (globalBoosters != null && !globalBoosters.isEmpty()) ? globalBoosters.get(0) : null;
        if(global != null && !global.isValid()) {
            globalBoosters.remove(0);
            return this.getGlobalBoost();
        } else {
            return global;
        }
    }

    public void addBoost(Player player, int percent, int mins) {
        Boost boost = new Boost(player.getName(), percent, getEndtime(mins));
        if(boosterMap.containsKey(player)) {
            RPGPlayer rp = plugin.getPlayer(player);
            String stored = rp.getSetting("BoostRemaining");
            stored = boost.serializeStored() + (stored != null ? ";" + stored : "");
            rp.setSetting("BoostRemaining", stored);
        } else {
            boosterMap.put(player, boost);
        }

    }

    public void addBoost(String causedby, int boost, int mins) {
        if(globalBoosters == null) {
            globalBoosters = new ArrayList<>(1);
        }

        globalBoosters.add(new Boost(causedby, boost, getEndtime(mins)));
    }

    public static class Boost {

        @Nullable private String username;
        private int boost;
        private long endTime;

        public Boost(String username, int boost, long endTime) {
            this.username = username;
            this.boost = boost;
            this.endTime = endTime;
        }

        public Boost(int boost, long endTime) {
            this.boost = boost;
            this.endTime = endTime;
        }

        public String getUsername() {
            return this.username;
        }

        public BoosterModule.Boost setUsername(String username) {
            this.username = username;
            return this;
        }

        public int getBoost() {
            return this.boost;
        }

        public BoosterModule.Boost setBoost(int boost) {
            this.boost = boost;
            return this;
        }

        public long getEndTime() {
            return this.endTime;
        }

        public boolean isValid() {
            return this.endTime > System.currentTimeMillis();
        }

        public String serializeStored() {
            return this.boost + "," + (this.endTime - System.currentTimeMillis());
        }

        public String serializeRunning() {
            return this.boost + "," + this.endTime;
        }

        public static BoosterModule.Boost deserializeStored(String raw) throws NumberFormatException {
            String[] split = raw.split(",");
            return new BoosterModule.Boost(Integer.parseInt(split[0]), System.currentTimeMillis() + Integer.parseInt(split[1]));
        }

        public static BoosterModule.Boost deserializeRunning(String raw) throws NumberFormatException {
            String[] split = raw.split(",");
            long endtime = Long.parseLong(split[1]);
            return endtime > System.currentTimeMillis() ? new BoosterModule.Boost(Integer.parseInt(split[0]), endtime) : null;
        }
    }

}
