package com.rpgme.content.module.expboost;

import com.rpgme.content.module.expboost.BoosterModule.Boost;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.command.CommandHelp;
import com.rpgme.plugin.command.CoreCommand;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.TimeUtils;
import com.rpgme.plugin.util.config.MessagesBundle;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by Robin on 30/07/2017.
 */
public class BoosterCommand extends CoreCommand {

    private static final String PERMISSION = "rpgme.command.boosters";
    private BoosterModule boosters = null;

    public BoosterCommand(RPGme plugin) {
        super(plugin, "rpgbooster");
        setConsoleAllowed(true);

        setAliases("expboost", "rpgboost");
        setDescription("This command can set an exp booster for all skills applied to a player or the whole server.");

        setCommandHelp(new CommandHelp.Builder()
                .addUsage("/expboost", "Personal overview of active exp boosts")
                .addUsage("/expboost set <player> <percentage> <minutes>", "Apply an exp boost to a specific player lasting a finite amount of time. Percentage is percentage of regular exp awarded (eg. 300 means x3 exp). If less than 100, exp gain will decrease.", PERMISSION)
                .addUsage("/expboost setglobal <percentage> <minutes> <cause>", "Add a exp boost for all players. The cause is displayed to players.", PERMISSION)
                .build());
    }

    private BoosterModule getBoosters() {
        if(boosters == null) {
            boosters = plugin.getModule(RPGme.MODULE_EXP_BOOSTS);
        }
        return boosters;
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {
        if(flags.isEmpty() || !hasPermission(sender, PERMISSION, false)) {
            sender.sendMessage(buildBoosterOverview(sender));
            return;
        }

        int percent;
        int duration;

        if(flags.remove("setglobal")) {
            if(flags.size() < 3) {
                error(sender, "Not enough arguments. /expboost setglobal <percentage> <minutes> <cause>");
                return;
            }

            try {
                percent = Integer.parseInt(flags.get(0));
                duration = Integer.parseInt(flags.get(1));
            } catch (NumberFormatException var10) {
                error(sender, "Wrong arguments, numbers expected. /expboost setglobal <percentage> <minutes> <cause>");
                return;
            }

            String cause = String.join(" ", flags.subList(2, flags.size()));
            getBoosters().addBoost(cause, percent, duration);
            sender.sendMessage(ChatColor.GREEN + "Global Multiplier set to " + toMultiplier(percent) + " for " + toDisplayTime(duration) + " by " + cause);

            for(Player player : plugin.getServer().getOnlinePlayers()) {
                player.sendMessage(buildBoosterOverview(player));
            }

        }
        else if(flags.remove("set")) {
            if(flags.size() < 3) {
                error(sender, "Not enough arguments. /expboost set <player> <percentage> <minutes>");
                return;
            }

            try {
                percent = Integer.parseInt(flags.get(1));
                duration = Integer.parseInt(flags.get(2));
            } catch (NumberFormatException var9) {
                error(sender, "Wrong arguments, numbers expected. /booster set <player> <percentage> <minutes>");
                return;
            }

            String name = flags.get(0);
            Player player = plugin.getServer().getPlayer(name);
            if(player == null) {
                error(sender, "Player not found (offline?) name=" + name);
            } else {
                getBoosters().addBoost(player, percent, duration);
                if(sender != player) {
                    sender.sendMessage(ChatColor.GREEN + "Booster activated for Player " + name + " of " + percent + "% for " + duration + "m");
                }
                player.sendMessage(buildBoosterOverview(player));
            }
        }
        else {
            printCommandHelp(sender);
        }
    }

    private String toMultiplier(int percent) {
        return "x" + percent / 100.0;
    }

    private String toDisplayTime(int minutes) {
        return TimeUtils.readableAsMinuteFormat(minutes * 60);
    }

    private String remainingDuration(long endTime) {
        long remaining = endTime - System.currentTimeMillis();
        return TimeUtils.readableAsMinuteFormat((int)Math.round(remaining / 1000.0));
    }

    public String buildBoosterOverview(CommandSender sender) {
        StringBuilder sb = new StringBuilder();
        MessagesBundle messages = plugin.getMessages();
        Boost global = getBoosters().getGlobalBoost();
        if(global != null) {
            sb.append("&e").append(" ║  ").append(messages.getMessage("ExpBoost_global", global.getUsername())).append(toMultiplier(global.getBoost())).append(" &7(").append(remainingDuration(global.getEndTime())).append(")\n&e");
        }

        Boost personal = null;
        if(sender instanceof Player) {
            Player boost = (Player)sender;
            personal = this.getBoosters().getBoost(boost);
            if(personal != null) {
                sb.append("&e").append(" ║  ").append(messages.getMessage("ExpBoost_personal")).append(toMultiplier(personal.getBoost())).append(" &7(").append(remainingDuration(personal.getEndTime())).append(")\n&e");
            }
        }

        if(sb.length() == 0) {
            sb.append("&7").append(" ║  ").append(messages.getMessage("ExpBoost_nothing")).append("\n&7");
        } else if(global != null && personal != null) {
            int boost1 = 100 + (global.getBoost() - 100) + (personal.getBoost() - 100);
            sb.append("&e").append(" ║  ").append("&7").append(messages.getMessage("skills_total")).append("&f&b").append(toMultiplier(boost1)).append("\n&e");
        }

        sb.insert(0, sb.subSequence(sb.length() - 2, sb.length()) + " ╔  " + "          &7-- &eRPGme exp Boosters &7--\n");
        sb.append(" ╚  ");
        return StringUtils.colorize(sb.toString());
    }

    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        return args.size() > 0 ? this.getOnlinePlayers() : null;
    }

}