package com.rpgme.content.event;

import org.bukkit.Material;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

public class SimpleAnvilRecipe implements AnvilRecipe {

	private ItemStack ingredient1, ingredient2, result;

    public SimpleAnvilRecipe(ItemStack ingredient1, ItemStack ingredient2, ItemStack result) {
        this.ingredient1 = ingredient1 == null ? new ItemStack(Material.AIR, 0) : ingredient1;
        this.ingredient2 = ingredient2 == null ? new ItemStack(Material.AIR, 0) : ingredient2;
        this.result = result == null ? new ItemStack(Material.AIR, 0) : result;
    }

    @Override
    public ItemStack getResult(InventoryView ingredients) {
		return result;
	}

    public AnvilRecipe setIngredients(ItemStack ingredient1, ItemStack ingredient2) {
        this.ingredient1 = ingredient1;
        this.ingredient2 = ingredient2;
        return this;
    }

    @Override
    public void removeIngredients(InventoryView view) {
        ItemStack item1 = view.getItem(0);
        ItemStack item2 = view.getItem(1);

        item1.setAmount(item1.getAmount() - ingredient1.getAmount());
        item2.setAmount(item2.getAmount() - ingredient2.getAmount());

        view.setItem(0, item1.getAmount() > 0 ? item1 : null);
        view.setItem(1, item2.getAmount() > 0 ? item2 : null);
    }

    public AnvilRecipe setResult(ItemStack result) {
        this.result = result;
        return this;
    }

    @Override
    public boolean matches(ItemStack[] ingredients) {
		return matches(ingredient1, ingredients[0]) && matches(ingredient2, ingredients[1]);
	}
	
	private boolean matches(ItemStack ingredient, ItemStack item) {
		return item.getAmount() >= ingredient.getAmount() && ingredient.isSimilar(item);
	}
	
	

}
