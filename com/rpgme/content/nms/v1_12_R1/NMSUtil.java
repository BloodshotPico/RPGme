package com.rpgme.content.nms.v1_12_R1;

import com.rpgme.content.nms.INMS.INMSUtil;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.EntityArrow;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.GenericAttributes;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;

public class NMSUtil implements INMSUtil {
	

	@Override
    public void setAbsorptionHearts(LivingEntity p, float extra) {
		((CraftLivingEntity)p).getHandle().setAbsorptionHearts(extra);
	}
	
	@Override
    public float getAbsorptionHearts(LivingEntity p) {
		return ((CraftLivingEntity)p).getHandle().getAbsorptionHearts();
	}

	@Override
    public void setInvisible(org.bukkit.entity.Entity entity, boolean value) {
		((CraftEntity)entity).getHandle().setInvisible(value);
	}
	
	@Override
    public void setMovementSpeed(org.bukkit.entity.Entity entity, double value) {
		Entity nms = ((CraftEntity)entity).getHandle();
		((EntityInsentient)nms).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(value);
	}
	
	@Override
    public boolean isFromMobspawner(org.bukkit.entity.Entity entity) {
		return ((CraftEntity)entity).getHandle().fromMobSpawner;
	}
	
	@Override
    public boolean canPickup(Arrow arrow) {
		return ((EntityArrow)((CraftEntity)arrow).getHandle()).fromPlayer == EntityArrow.PickupStatus.ALLOWED;
	}
	
	@Override
    public void setCanPickup(Arrow arrow, boolean value) {
		((EntityArrow)((CraftEntity)arrow).getHandle()).fromPlayer = EntityArrow.PickupStatus.ALLOWED;
	}


}
