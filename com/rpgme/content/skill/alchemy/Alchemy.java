package com.rpgme.content.skill.alchemy;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.Id;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.BrewEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Alchemy extends BaseSkill {
	
	private static final Set<PotionEffectType> dangerous = Sets.newHashSet(PotionEffectType.POISON, PotionEffectType.SLOW, PotionEffectType.WEAKNESS, PotionEffectType.HARM);
	
	public static boolean isNegativeEffect(PotionEffectType type) {
		return dangerous.contains(type);
	}

	public static final int ABILITY_POTION_CLOUD = Id.newId();
	public static final int ABILITY_POTION_SPLASH = Id.newId();
	public static final int ABILITY_SAFETY_FIRST = Id.newId();

	private final Map<Location, UUID> brewMap = Maps.newHashMap();

	public Alchemy () {
		super("Alchemy", SkillType.ALCHEMY);
	}

	@Override
	public void onEnable() {
		registerAbility(ABILITY_POTION_CLOUD, new PotionCloud(this));
		registerAbility(ABILITY_POTION_SPLASH, new Splash(this));
		registerAbility(ABILITY_SAFETY_FIRST, new SafetyFirst(this));
	}

	@Override
	public Material getItemRepresentation() {
		return Material.LINGERING_POTION;
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBrew(BrewEvent e) {

		Location loc = e.getBlock().getLocation();
		Player p = plugin.getServer().getPlayer(brewMap.get(loc));

		if(e.getContents().getIngredient().getAmount() == 1) {
			brewMap.remove(loc);
		}

		GameSound.play(Sound.ENTITY_PLAYER_BURP, loc);
		doBrewParticles(loc);

		if(p != null && isEnabled(p)) {
			int xp = ExpTables.getAlchemyExp(e.getContents().getIngredient().getType()) * getPotionAmount(e);
			addExp(p, xp);

		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onBrewStart(InventoryClickEvent e) {
		if(e.getInventory().getType() != InventoryType.BREWING) {
			return;
		}
		
		if(!isEnabled((Player)e.getWhoClicked()))
			return;

		// regular clicking in the ingredient slot of the brewing stand
		if(e.getSlotType() == SlotType.FUEL) {
			if(e.getAction() == InventoryAction.PLACE_ALL || e.getAction() == InventoryAction.PLACE_ONE || e.getAction() == InventoryAction.PLACE_SOME) {

				brewMap.put(getLocation(e.getInventory().getHolder()), e.getWhoClicked().getUniqueId());
			} else if(e.getAction() == InventoryAction.PICKUP_ALL || e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) {

				brewMap.remove(getLocation(e.getInventory().getHolder()));
			}

			// shift clicking in your inv to put an ingredient in the stand
		} else if(e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY && e.getCurrentItem().getType() != Material.POTION) {

			brewMap.put(getLocation(e.getInventory().getHolder()), e.getWhoClicked().getUniqueId());
		}
	}
	

	
	private Location getLocation(InventoryHolder holder) {
		return ((BlockState)holder).getLocation();
	}

	private int getPotionAmount(BrewEvent e) {
		int amount = 0;
		for(ItemStack item : e.getContents().getContents()) {
			if(item != null && item.getType() == Material.POTION)
				amount++;
		}
		return amount;
	}

	private void doBrewParticles(Location loc) {
		Vector up = new Vector(0,1,0);
		for(Location l : CoreUtils.circle(loc.add(0.5, 0.5, 0.5), 0.55, 3)) {
            l.getWorld().spawnParticle(Particle.FLAME, l, 1, 0.0, 0.0, 0.0, 0.1);
		}
	}

}
