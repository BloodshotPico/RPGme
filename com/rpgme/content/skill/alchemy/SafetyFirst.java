package com.rpgme.content.skill.alchemy;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;

import static com.rpgme.content.skill.alchemy.Alchemy.isNegativeEffect;

/**
 * Created by Robin on 28/06/2016.
 */
public class SafetyFirst extends Ability<Alchemy> {

    private int safetyUnlock;

    public SafetyFirst(Alchemy skill) {
        super(skill, "Safety First", Alchemy.ABILITY_SAFETY_FIRST);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 30);
        ConfigHelper.injectNotification(messages, getClass(), "");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        safetyUnlock = config.getInt("unlocked");

        addNotification(safetyUnlock, Notification.ICON_PASSIVE, getName(), "From now on you will no longer be effected by negative potion-effects from your own Splash Potions.");
    }

    public int getUnlockLevel() {
        return safetyUnlock;
    }

    // safety first
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPotionSplash(PotionSplashEvent e) {
        if(e.getPotion().getShooter() instanceof Player) {
            Player p = (Player) e.getPotion().getShooter();
            if(!isEnabled(p))
                return;
            int level = getLevel(p);
            if(level >= safetyUnlock) {
                for(PotionEffect eff : e.getPotion().getEffects()) {
                    if(isNegativeEffect(eff.getType()))
                        e.setIntensity(p, 0.0);
                }
            }
        }

    }
}
