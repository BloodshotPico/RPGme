package com.rpgme.content.skill.alchemy;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.util.List;

public class Splash extends Ability<Alchemy> {
	
	private int unlock;
	
	public Splash(Alchemy skill) {
		super(skill, "Splash", Alchemy.ABILITY_POTION_SPLASH);
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlock) {
			list.add("Splash Potion radius:"+ StringUtils.readableDecimal(getSplashRadius(forlevel)));
		}
		
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 15);
		ConfigHelper.injectNotification(messages, getClass(), "");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		addNotification(unlock, Notification.ICON_PASSIVE, getName(), ConfigHelper.getNotification(messages, getClass(), ""));
	}

	private double getSplashRadius(int atlevel) {
		return Math.min(((atlevel / 20) * 1.3) + 4.5, 12);
	}

	@EventHandler(ignoreCancelled = true)
	public void onSplashPotion(PotionSplashEvent e) {
		ThrownPotion projectile = e.getPotion();
		if(projectile.getShooter() instanceof Player) {

			Player p = (Player)projectile.getShooter();
			int level = getLevel(p);

			if(level > unlock) {		

				e.setCancelled(true);
				double radius = getSplashRadius(level);

				for(Entity entity : projectile.getNearbyEntities(radius, radius, radius)) {
					if(entity instanceof LivingEntity) {

						for(PotionEffect eff : projectile.getEffects()) {

							SafetyFirst safety = skill.getAbility(Alchemy.ABILITY_SAFETY_FIRST);
							if(entity == p && Alchemy.isNegativeEffect(eff.getType()) && getLevel((Player) p) >= safety.getUnlockLevel())
								continue;
							
							eff.apply((LivingEntity)entity);
						}
					}
				}

				double speed = radius * (1.0 / 12) * 0.6;
				doPotionParticles(projectile.getLocation(), projectile, level, speed);
			}
		}
	}
	@SuppressWarnings("deprecation")
	private void doPotionParticles(Location center, ThrownPotion potion, int playerLvl, double speed) {

		double grow = (playerLvl/20) * 0.5;

		int amount = (int) Math.ceil(16 + grow);

		List<Vector> vectors = CoreUtils.outwardsVectors(amount);
		List<Location> circle = CoreUtils.circle(center, 3+(grow * 0.75), amount);

		for(int i = 0; i < amount; i++) {

			Vector dir = vectors.get(i);
			Location loc = CoreUtils.addNaturalOffset(circle.get(i).toVector(), 1.2).toLocation(center.getWorld());

			int abilitylvl = (playerLvl/20);

			if(abilitylvl == 1) {

                loc.getWorld().spawnParticle(Particle.SMOKE_LARGE, loc, 1, dir.getX(), dir.getY(), dir.getX(), 0.2);

			}
			else {

				try {
					Potion pot = Potion.fromItemStack(potion.getItem());
					center.getWorld().spigot().playEffect(loc, Effect.POTION_BREAK, pot.getNameId(), pot.toDamageValue(), 3f, 0.4f, 3f, 0.5f, 1, 24);
				} catch(IllegalArgumentException e) {

					center.getWorld().spigot().playEffect(loc, Effect.INSTANT_SPELL, 0, 0,  3f, 0.2f, 3f, 0.5f, 10, 24);

				}
			}

		}
	}
}