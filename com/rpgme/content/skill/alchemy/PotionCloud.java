package com.rpgme.content.skill.alchemy;

import com.google.common.collect.Lists;
import com.rpgme.content.nms.NMS;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.Scaler;
import com.rpgme.plugin.util.nbtlib.ByteTag;
import com.rpgme.plugin.util.nbtlib.CompoundTag;
import com.rpgme.plugin.util.nbtlib.NBTFactory;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class PotionCloud extends Ability<Alchemy> {
	
	private static final String NBT_KEY = "PotionCloud";
	
	public static ItemStack createCloudPotion(PotionType type) {
	    if(!type.isUpgradeable())
	        throw new IllegalArgumentException("Type is not upgradable");

		ItemStack pot = ItemUtils.create(new ByteTag(NBT_KEY, (byte)0), Material.POTION, null, null, null);
		new Potion(type, 2).splash().apply(pot);
		return pot;
	}
	public static boolean isCloudPotion(ItemStack item) {
		CompoundTag tag = NBTFactory.getFrom(item);
		return tag != null && tag.containsKey(NBT_KEY);
	}

	private int unlock;
	private Scaler chance, duration;

	public PotionCloud(Alchemy skill) {
		super(skill, "Potion Cloud", Alchemy.ABILITY_POTION_CLOUD);
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 20);

		ConfigHelper.injectNotification(messages, getClass(), 1);
		ConfigHelper.injectNotification(messages, getClass(), 2);
		ConfigHelper.injectNotification(messages, getClass(), 3);
		ConfigHelper.injectNotification(messages, getClass(), 4);
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		unlock = config.getInt("unlocked");

		int targetLvl = getPlugin().getSkillManager().getTargetLevel();
		int increase = targetLvl / 5;
		addNotification(unlock, Notification.ICON_UNLOCK, "Potion Cloud", ConfigHelper.getNotification(messages, getClass(), 1));
		addNotification(Math.min(targetLvl, unlock+increase), Notification.upgradableIcon(1, 3), "Potion Cloud", ConfigHelper.getNotification(messages, getClass(), 2));
		addNotification(Math.min(targetLvl, unlock+increase*2), Notification.upgradableIcon(2, 3), "Potion Cloud", ConfigHelper.getNotification(messages, getClass(), 3));
		addNotification(Math.min(targetLvl, unlock+increase*3), Notification.upgradableIcon(3, 3), "Potion Cloud", ConfigHelper.getNotification(messages, getClass(), 4));

		chance = new Scaler(unlock, 10, 100, 100);
		duration = new Scaler(unlock, 6, 100, 12);
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlock) {
			list.add("Potion Cloud chance:"+(int)chance.scale(forlevel)+'%');
			list.add("Potion Cloud duration:"+(int) duration.scale(forlevel)+'s');
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onPotionSplash(PotionSplashEvent e) {

		ProjectileSource source = e.getEntity().getShooter();
		if(source instanceof Player) {

			Player player = (Player) source;
			if(isCloudPotion(e.getPotion().getItem())) {
				e.setCancelled(true);
				startPotionCloud(e.getEntity(), 75);
				return;
			}
			
			if(!isEnabled(player))
				return;
			int level = getLevel(player);

			if(level < unlock || !chance.isRandomChance(level))
				return;

			e.setCancelled(true);
			startPotionCloud(e.getEntity(), level);

		}
	}
	
	@EventHandler (priority = EventPriority.LOW)
	public void noBatDamage(EntityDamageEvent e) {
		if(e.getEntityType() == EntityType.BAT && e.getCause() == DamageCause.MAGIC)
			e.setCancelled(true);
	}
	
	private void startPotionCloud(ThrownPotion potion, int level) {
		int size = 20;
		int duration = (int) this.duration.scale(level);	
		
		int maxDuration = (level < 60 ? 15 : (level < 80 ? 20 : (level < 99 ? 30 : -1)) ) * 60 * 20;
		
		startPotionCloud(potion.getLocation(), potion, size, duration, maxDuration);
	}
	
	
	
	private final PotionEffect invis = new PotionEffect(PotionEffectType.INVISIBILITY, 60*20, 0, true, false);

	public void startPotionCloud(Location loc, ThrownPotion potion, int size, int duration, int maxDuration) {

		List<Entity> bats = Lists.newArrayList();

		for(int i = 0; i < size; i++) {

			Bat bat = loc.getWorld().spawn(loc, Bat.class);
			NMS.util.setMovementSpeed(bat, 0.05);
			bat.addPotionEffect(invis);
			bats.add(bat);

		}
		
		new CloudTask(potion, bats, duration, maxDuration).start(plugin);
	}

	private class CloudTask extends BukkitRunnable {

		final ThrownPotion potion;
		final List<Entity> bats;
		final int repetitions, maxDuration;

		int i = 0;

		public CloudTask(ThrownPotion potion, List<Entity> bats, int seconds, int maxDuration) {
			this.potion = potion;
			this.bats = bats;
			this.maxDuration = maxDuration;
			this.repetitions = (int) (seconds*(20.0/8));
		}

		public void start(RPGme plugin) {
			runTaskTimer(plugin, 0l, 8l);
		}

		@Override
		public void run() {
			if(++i == repetitions) {
				cancel();

				for(Entity e : bats)
					e.remove();

			}
			else {
				
				boolean sound = CoreUtils.random.nextBoolean();
				
				for(int i = 0; i < bats.size(); i++) {
					
					Entity bat = bats.get(i);
					
					for(Entity e : bat.getNearbyEntities(2.5, 2.5, 2.5)) {
						
						if(e.getType() == EntityType.BAT || !(e instanceof LivingEntity))
							continue;
						
						LivingEntity p = (LivingEntity)e;

						// potion effect
						for(PotionEffect effect : potion.getEffects()) {
							
							if(effect.getType().isInstant()) {
								
								if(effect.getType().equals(PotionEffectType.HEAL)) {
									double heal = (effect.getAmplifier()+1)*0.2;
									p.setHealth(Math.min(p.getMaxHealth(), p.getHealth()+heal));
								} else { // equals damage
									double damage = (effect.getAmplifier()+1)/2;
									p.damage(damage, potion);
								}
							} else {

								SafetyFirst safety = skill.getAbility(Alchemy.ABILITY_SAFETY_FIRST);
								if(e == potion.getShooter() && Alchemy.isNegativeEffect(effect.getType()) && getLevel((Player) p) >= safety.getUnlockLevel())
									continue;
								
								int duration = effect.getDuration()/5;
								for(PotionEffect current : p.getActivePotionEffects()) {
									if(current.getType().equals(effect.getType())) {
										duration += current.getDuration();
										break;
									}
								}
								if(maxDuration > 0)
									duration = Math.min(maxDuration, duration);
								
								p.addPotionEffect(effect.getType().createEffect(duration, effect.getAmplifier()), true);
								
							}
							
						}
					}
					
					// effect
					if(sound) {
						GameSound.play(Sound.ENTITY_CAT_HISS, bat.getLocation(), 0.6f, 0.7f, 0.5);
					}
					
					if(i % 2 == 0)
						doPotionParticles(bat.getLocation(), potion);
					
				}
				
			}
		}
	}

	@SuppressWarnings("deprecation")
	public static void doPotionParticles(Location center, ThrownPotion potion) {
        World world = center.getWorld();
		for(PotionEffect effect : potion.getEffects()) {
			
			PotionEffectType type = effect.getType();
			
			if(type.equals(PotionEffectType.HEAL)) {

                world.spawnParticle(Particle.VILLAGER_HAPPY, center, 5, 3.0, 1.5, 3.0, 0.15);
                world.spawnParticle(Particle.HEART, center, 1, 1.5, 1.5, 1.5, 1.0);
				
			}
			else if(type.equals(PotionEffectType.HARM)) {

                world.spawnParticle(Particle.REDSTONE, center, 30, 1.0, 1.0, 1.0, 0.0);
                world.spawnParticle(Particle.FLAME, center, 2, 1.5, 1.5, 1.5, 0.08);
				
			}
			else if(type.equals(PotionEffectType.POISON)) {

                world.spawnParticle(Particle.SPELL_WITCH, center, 20, 1.5, 1.5, 1.5, 0.2);
                world.spawnParticle(Particle.FLAME, center, 2, 1.5, 1.5, 1.5, 0.05f);
				
			} 
			else if(type.equals(PotionEffectType.INVISIBILITY)) {

                world.spawnParticle(Particle.CLOUD, center, 20, 3.0, 1.5, 3.0, 0.05);
                world.spawnParticle(Particle.CLOUD, center, 2, 1.5, 1.5, 1.5, 0.1);
				
			} else {
				
				Potion pot = Potion.fromItemStack(potion.getItem());
				
				if(!type.isInstant()) {
					center.getWorld().spigot().playEffect(center, Effect.POTION_BREAK, pot.getNameId(), pot.toDamageValue(), 3f, 0.4f, 3f, 0.5f, 1, 24);
				} else {
					center.getWorld().spigot().playEffect(center, Effect.INSTANT_SPELL, pot.getNameId(), pot.toDamageValue(),  3f, 0.2f, 3f, 0.5f, 10, 24);
				}
				
			}
			
		}
		
	}
	
	




}
