package com.rpgme.content.skill.woodcutting;

import com.rpgme.plugin.blockmeta.PlayerPlacedListener;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.cooldown.Cooldown;
import com.rpgme.plugin.util.cooldown.VarEnergyPerSecCooldown;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Timber extends Ability<Woodcutting> {

	private static final Vector[] checkArea;
	static {
		List<Vector> list = new ArrayList<>();
		for(int x = -1; x <= 1; x++) {
			for(int y = 1; y >= -1; y--) {
				for(int z = -1; z <= 1; z++) {
					list.add(new Vector(x,y,z));
				}
			}
		}
		list.remove(new Vector());
		checkArea = list.toArray(new Vector[list.size()]);
	}

	private static final int ENERY_COST = 40;

	private final Cooldown cooldown = new VarEnergyPerSecCooldown(plugin, ENERY_COST, 100);
	private Scaler cooldownSpeed;

	private int unlock, blockLimit;

	public Timber(Woodcutting skill) {
		super(skill, "Timber", Woodcutting.ABILITY_TIMBER);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("unlocked", 15)
                .addValue("Maximum amount of blocks per tree, including leaves", "max size", 450);
        ConfigHelper.injectNotification(messages, getClass(), "");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        unlock = getConfig().getInt("unlocked");
        blockLimit = getConfig().getInt("max size");
        cooldownSpeed = new Scaler(unlock, 1, 100, 6);

        addNotification(unlock, Notification.ICON_UNLOCK, getName(), ConfigHelper.getNotification(messages, getClass(), ""));
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlock) {
			list.add("Timber Max cooldown:" + StringUtils.readableDecimal(ENERY_COST / cooldownSpeed.scale(forlevel))+"s");
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onAbility(PlayerInteractEvent e) {

		if(e.getAction() == Action.RIGHT_CLICK_BLOCK && 
				e.hasItem() && isAxe(e.getItem().getType()) && Woodcutting.isWood((e.getClickedBlock().getType())) &&
				!PluginIntegration.getInstance().isInClaim(e.getClickedBlock())) {

			Player p = e.getPlayer();

			if(!isEnabled(p))
				return;

			int level = getLevel(p);

			if(level >= unlock) {

				if(cooldown.isOnCooldown(p)) {
					sendOnCooldownMessage(p, cooldown, true);
					return;
				}

				if(!PluginIntegration.getInstance().canChange(p, e.getClickedBlock())) {
					p.sendMessage(getPlugin().getMessages().getMessage("err_nopermission"));
					GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
					return;
				}

                if(dropTree(e.getClickedBlock(), e.getPlayer())) {
                    long energypersec = (long) cooldownSpeed.scale(level)*1000;
                    cooldown.add(p, energypersec);
                } else {
                    GameSound.play(Sound.ENTITY_VILLAGER_NO, p);
                }

			}

		}
	}

	private boolean dropTree(Block start, Player player) {

		Set<Block> toDrop = new HashSet<>();
		Set<Block> toCheck = new HashSet<>();
        Set<Block> toAdd = new HashSet<>();

		toCheck.add(start);

		boolean done = false;
        int maxHeight = 0;

		while(!done) {

			for(Block b : toCheck) {

				if(toDrop.contains(b))
					continue;

				if(!PluginIntegration.getInstance().canChange(player, b))
					continue;

				for(Vector dir : checkArea) {

					Block other = b.getLocation().add(dir).getBlock();

                    if(!PluginIntegration.getInstance().canChange(player, other))
                        continue;

                    if(Woodcutting.isBush(other.getType())) {
                        toAdd.add(other);
                    }
					else if(Woodcutting.isWood(other.getType()) && !PlayerPlacedListener.getInstance().isPlayerPlaced(other)) {
						toAdd.add(other);
                        maxHeight = Math.max(maxHeight, other.getY() - start.getY() + 1);
					}
					else if(shouldDestroy(other.getType())) {
					    other.breakNaturally();
                    }
				}

				toDrop.add(b);

			}

			toCheck.addAll(toAdd);
            toAdd.clear();

			if(toDrop.size() >= blockLimit || toDrop.containsAll(toCheck)) {
                done = true;
            }

		}

		if(maxHeight < 4) {
		    return false;
        }

		//		skill.setSleeping(true);
		Vector dir = Vector.getRandom().normalize().setY(0.3);
        toDrop.remove(start);
        start.breakNaturally();
		int woodblocks = 1;

		for(Block b : toDrop) {

            BlockState state = b.getState();

			if(Woodcutting.isBush(state.getType())) {

				//	LeavesDecayEvent event = new LeavesDecayEvent(b);
				//	manager.getServer().getPluginManager().callEvent(event);

				if(b.getRelative(BlockFace.UP).getType() == Material.SNOW) {
					b.getRelative(BlockFace.UP).setType(Material.AIR);
				}

				if(CoreUtils.random.nextDouble() < 0.12) {

				    b.setType(Material.AIR);

					int height = b.getY() - start.getY() + 1;
					double speed = 0.3 + Math.min(height * 0.04, 1.0);
                    FallingBlock block = b.getWorld().spawnFallingBlock(b.getLocation(), state.getData());
                    block.setVelocity(dir.clone().multiply(speed));
                    /* There seems to be a bug with FallingBlock where it always sets the state to 'no decay',
                       causing invalid texture for the dropped item */
                    if(state.getType() == Material.LEAVES_2) {
                        block.setDropItem(false);
                    }
				} else {
				    b.breakNaturally();
                }


			} else {

				//	BlockBreakEvent event = new BlockBreakEvent( b, player);
				//	manager.getServer().getPluginManager().callEvent(event);
				woodblocks++;
				GameSound.play(Sound.BLOCK_WOOD_BREAK, b.getLocation());

				b.setType(Material.AIR);

				int height = b.getY() - start.getY() + 1;
                double speed = 0.3 + Math.min(height * 0.04, 1.0);
                FallingBlock block = b.getWorld().spawnFallingBlock(b.getLocation(), state.getData());
                block.setVelocity(dir.clone().multiply(speed));
			}

		}

		float xp = woodblocks * 2.5f;
		addExp(player, xp);
        return true;
	}

	private boolean isAxe(Material mat) {
		return mat.name().endsWith("_AXE");
	}

	private boolean shouldDestroy(Material mat) {
	    return mat == Material.VINE || mat == Material.COCOA;
    }

}
