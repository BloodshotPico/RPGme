package com.rpgme.content.skill.woodcutting;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.blockmeta.BlockDataManager;
import com.rpgme.plugin.blockmeta.PlayerPlacedListener;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.effect.PotionEffectUtil;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.potion.PotionEffectType;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class Woodcutting  extends BaseSkill {

	private static final Set<Material> WOOD_BLOCKS = EnumSet.of(Material.LOG, Material.LOG_2, Material.HUGE_MUSHROOM_1, Material.HUGE_MUSHROOM_2);

	public static boolean isBush(Material mat) {
		return mat == Material.LEAVES || mat == Material.LEAVES_2;
	}

	public static boolean isWood(Material mat) {
		return WOOD_BLOCKS.contains(mat);
	}

	public static int ABILITY_TIMBER = Id.newId();

	private final Scaler treasureChance = new Scaler(10,0.2 ,100, 2);
	private int hasteUnlock1, hasteUnlock2;

    private PlayerPlacedListener playerPlacedListener;

	public Woodcutting() {
		super("Woodcutting", SkillType.WOODCUTTING);
	}

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder bundle) {
        super.createConfig(config, bundle);
        config.addValue("haste 1", 40)
                .addValue("haste 2", 80);
        ConfigHelper.injectMessage(bundle, "notification_haste1");
        ConfigHelper.injectMessage(bundle, "notification_haste2");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        hasteUnlock1 = config.getInt("haste 1");
        hasteUnlock2 = config.getInt("haste 2");
        if(hasteUnlock1 > -1) {
            addNotification(hasteUnlock1, Notification.ICON_PASSIVE, "Hasteful Gathering", messages.getMessage("notification_haste1"));
            if(hasteUnlock2 > -1) {
                addNotification(hasteUnlock2, Notification.ICON_PASSIVE, "Hasteful Gathering", messages.getMessage("notification_haste2"));
            }
        }
    }

    @Override
    public Material getItemRepresentation() {
        return Material.STONE_AXE;
    }

    @Override
    public void onEnable() {
        super.onEnable();
        registerAbility(ABILITY_TIMBER, new Timber(this));

        BlockDataManager blockManager = getPlugin().getModule(RPGme.MODULE_BLOCK_META);
        playerPlacedListener = blockManager.getPlayerPlacedListener();
        playerPlacedListener.registerMaterial(Material.LOG, Material.LOG_2);
    }

    @Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= treasureChance.minlvl)
			list.add("Treasure Chance:"+ treasureChance.readableScale(forlevel) + "%");
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onTreeCut(BlockBreakEvent e) {		
		Player p = e.getPlayer();

		if(!isEnabled(p))
			return;

		int exp = ExpTables.getWoodcuttingExp(e.getBlock());

		if(exp > 0) {

			if(playerPlacedListener.collectPlayerPlaced(e.getBlock()))
				return;

			addExp(p, exp);

			// hastefull gathering
			if(hasteUnlock1 >= 0) {
				int level = getLevel(p);
				int tier = level >= hasteUnlock2 ? 2 : level >= hasteUnlock1 ? 1 : 0;
				if(tier > 0)
					PotionEffectUtil.addPotionEffect(p, PotionEffectType.FAST_DIGGING, tier, 3, false, false);
			}

		} else if(isBush(e.getBlock().getType() ) && !ItemUtils.isType(p.getInventory().getItemInMainHand(), Material.SHEARS)) {

			int level = getLevel(p);
			if(treasureChance.isRandomChance(level)) {
				TreasureBag.getInstance().spawnTreasure(e.getBlock(), level);
			}

		}
	}

}
