package com.rpgme.content.skill.defence;

import com.google.common.collect.Maps;
import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Map;

public class BulkUp extends Ability<Defence> {

    private static final int POTIONEFFECT_DURATION = 20 * 60 * 10; // 10min

	private int unlock, delay;
	private Scaler chargePeriod, maxCharge;

	private final Map<Player, Charge> chargeMap = Maps.newHashMap();

	public BulkUp(Defence defence) {
		super(defence, "Bulk Up", Defence.ABILITY_BULK_UP);
	}

	@Override
	public void createConfig(ConfigBuilder config, BundleBuilder messages) {
		super.createConfig(config, messages);
		config.addValue("unlocked", 20)
				.addValue("max hearts", 10)
				.addValue("charge delay", 80);

		ConfigHelper.injectNotification(messages, getClass(), "");
	}

	@Override
	public void onLoad(ConfigurationSection config, BundleSection messages) {
		super.onLoad(config, messages);
		unlock = config.getInt("unlocked");
		delay = getConfig().getInt("charge delay");
		int max = Math.max(2, getConfig().getInt("max hearts"));
        int target = skill.getManager().getTargetLevel();
		chargePeriod = new Scaler(unlock, 60, target, 20);
		maxCharge = new Scaler(unlock, 2, target, max);

		addNotification(unlock, Notification.ICON_UNLOCK, getName(),
				ConfigHelper.getNotification(messages, getClass(), "", delay/20));
	}

	@Override
	public void addCurrentStatistics(int forlevel, List<String> list) {
		if(forlevel >= unlock) {
			list.add("Max Bulk Up:"+(int)maxCharge.scale(forlevel)+" hearts");
		}
	}
	
	@EventHandler
	public void onBlock(PlayerInteractEvent e) {
		if(!CoreUtils.isRightClick(e.getAction()) || !e.hasItem() || e.getItem().getType() != Material.SHIELD)
			return;
		Player p = e.getPlayer();
		
		if(!isEnabled(p))
			return;
		
		int level = getLevel(p);
		if(level < unlock)
			return;
				
		Charge charge = chargeMap.get(p);
		if(charge != null) {
			charge.cancel();
		}
		
		int max = (int) maxCharge.scale(level);
		long period = Math.round(chargePeriod.scale(level));
		
		charge = new Charge(p, max);
		charge.start(period);
		
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		remove(e.getPlayer());
	}
	@EventHandler
	public void onKick(PlayerKickEvent e) {
		remove(e.getPlayer());
	}

	private void remove(Player p) {
		Charge c = chargeMap.remove(p);
		if(c != null) {
			c.cancel();
		}
	}

	private class Charge extends BukkitRunnable {

		final Player p;
		final int max;
		int current = 0;

		Charge(Player p, int max) {
			this.max = max;
			this.p = p;
		}
		
		public Charge start(long period) {
			runTaskTimer(plugin, delay, period);
			chargeMap.put(p, this);
			GameSound.play(Sound.ITEM_ARMOR_EQUIP_GENERIC, p);
			return this;
		}

		public void run() {			
			if(!p.isBlocking()) {
				cancel();
				return;
			}

            if(p.hasPotionEffect(PotionEffectType.ABSORPTION)) {
                p.getActivePotionEffects().stream().filter(effect -> effect.getType() == PotionEffectType.ABSORPTION).forEach(effect -> {
                    current = effect.getAmplifier();
                });
            }
            //			if(current < 0) {
//				current = (int) NMS.util.getAbsorptionHearts(p)/2;
//			}

			current++;
			
			if(current/2 > max) {
				cancel();
				return;
			}

            p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, POTIONEFFECT_DURATION, current, true, false));
			//NMS.util.setAbsorptionHearts(p, current * 2 );
            p.getWorld().spawnParticle(Particle.SNOW_SHOVEL, p.getEyeLocation(), 8, 0.1, 0.1, 0.1, 0.07);

			if(current/2 == max) {
				cancel();
				GameSound.play(Sound.BLOCK_NOTE_PLING, p.getLocation(), 1f, 2f);
			} else {
				GameSound.play(Sound.BLOCK_NOTE_PLING, p.getLocation(), 0.5f, 1.5f);
			}
		}
		
		@Override
		public void cancel() {
			super.cancel();
			chargeMap.remove(this);
		}

	}

}
