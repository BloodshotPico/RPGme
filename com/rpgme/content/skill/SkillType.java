package com.rpgme.content.skill;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.util.Id;

import java.util.List;

/**
 *
 */
public final class SkillType {

    // combat
    public static final int ATTACK = Id.newId();
    public static final int DEFENCE = Id.newId();
    public static final int ARCHERY = Id.newId();
    public static final int MAGIC = Id.newId();

    // gathering
    public static final int MINING = Id.newId();
    public static final int LANDSCAPING = Id.newId();
    public static final int WOODCUTTING = Id.newId();
    public static final int FISHING = Id.newId();
    public static final int FARMING = Id.newId();

    // crafting
    public static final int FORGING = Id.newId();
    public static final int ALCHEMY = Id.newId();
    public static final int ENCHANTING = Id.newId();

    // other
    public static final int STAMINA = Id.newId();
    public static final int TAMING = Id.newId();
    //public static final int SUMMONING = Id.newId();

    public static int[] values() {
        List<Integer> list = RPGme.getInstance().getSkillManager().getKeys();
        int[] array = new int[list.size()];
        for(int i = 0;i < array.length;i++)
            array[i] = list.get(i);
        return array;
    }

    /**
     * Returns the id of a skill by name.
     * This goes into the skillmanager to getEntry skills, so the id does not necessarily have to be declared in this class.
     * @param name the name of skill. case-independent and can be either final name or displayname
     * @return the id if found, or -1 if not.
     */
    public static int valueOf(String name) {
        Skill skill = valueOfSkill(name);
        return skill != null ? skill.getId() : -1;
    }

    /**
     * Returns a skill by name.
     * This goes into the skillmanager to getEntry skills, so the id does not necessarily have to be declared in this class.
     * @param name the name of skill. case-independent and can be either final name or displayname
     * @return the skill if found, or null if not.
     */
    public static Skill valueOfSkill(String name) {
        if(name == null || name.isEmpty())
            return null;

        for(Skill skill : RPGme.getInstance().getSkillManager().getEnabledSkills()) {
            if(name.equalsIgnoreCase(skill.getName()) || (skill.hasDisplayName() && name.equalsIgnoreCase(skill.getDisplayName()))) {
                return skill;
            }
        }
        return null;
    }
    
}
