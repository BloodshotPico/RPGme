package com.rpgme.content.skill.mining;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.blockmeta.PlayerPlacedListener;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.skill.BaseSkill;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.util.CoreUtils;
import com.rpgme.plugin.util.Id;
import com.rpgme.plugin.util.StringUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.config.ConfigHelper;
import com.rpgme.plugin.util.effect.PotionEffectUtil;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import java.util.EnumSet;
import java.util.List;

public class Mining extends BaseSkill implements PowerTool.BlockBreaker {

    /** Blocks that can be broken with the power tool ability (eg all 'minable' blocks) */
    private static final EnumSet<Material> POWER_TOOL_BLOCKS = EnumSet.of(Material.STONE, Material.NETHERRACK,
            Material.MOSSY_COBBLESTONE, Material.QUARTZ_BLOCK, Material.GLOWSTONE, Material.NETHER_BRICK,
            Material.COBBLESTONE, Material.SMOOTH_BRICK, Material.PRISMARINE, Material.BRICK, Material.HARD_CLAY,
            Material.STAINED_CLAY, Material.COAL_ORE, Material.IRON_ORE, Material.REDSTONE_ORE, Material.GOLD_ORE,
            Material.LAPIS_ORE, Material.ENDER_STONE, Material.DIAMOND_ORE, Material.EMERALD_ORE);

    /** Blocks that normally drop exp, but shouldn't when used with a silk touch pickaxe because it would lead to infinite exp with one ore. */
    private static final EnumSet<Material> SILK_TOUCH_BLOCKS = EnumSet.of(Material.COAL_ORE, Material.DIAMOND_ORE,
            Material.LAPIS_ORE, Material.REDSTONE_ORE, Material.QUARTZ_ORE, Material.EMERALD_ORE);


    private Scaler autosmeltChance = new Scaler(9,0,120,100);
    private Scaler treasureChance = new Scaler(20,0.25,100,1);
    private int hasteUnlock1, hasteUnlock2;

    public static final int ABILITY_POWER_TOOL = Id.newId();

    public Mining() {
        super("Mining", SkillType.MINING);
    }

    @Override
    public Material getItemRepresentation() {
        return Material.STONE_PICKAXE;
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if(forlevel >= treasureChance.minlvl)
            list.add("Treasure Chance:"+ StringUtils.readableDecimal(treasureChance.scale(forlevel)) + '%');
        if(forlevel > autosmeltChance.minlvl)
            list.add("Autosmelt Chance:"+autosmeltChance.readableScale(forlevel)+"%");
    }

    @Override
    public void onEnable() {
        registerAbility(ABILITY_POWER_TOOL, new PowerTool<>(this, "PICKAXE", POWER_TOOL_BLOCKS, new int[] {180, 60}, new int[] {10, 45}));
        PlayerPlacedListener.getInstance().registerMaterial(Material.IRON_ORE, Material.GOLD_ORE);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        config.addValue("haste 1", 50)
                .addValue("haste 2", 100);

        messages.addValue("treasure", "You now have a small chance of finding treasure chests while mining. This chance will increase further as you level up.");
        messages.addValue("autosmelt", "Mining iron and gold ore now has a chance to auto-smelt the ores. The chance this will happen will increase as you level up.");
        ConfigHelper.injectMessage(messages, "notification_haste1");
        ConfigHelper.injectMessage(messages, "notification_haste2");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);

        new AutosmeltCommand(plugin).register();

        addNotification(10, Notification.ICON_UNLOCK, "Autosmelt", messages.getMessage("autosmelt"));
        addNotification(treasureChance.minlvl, Notification.ICON_PASSIVE, "Treasure", messages.getMessage("treasure"));

        hasteUnlock1 = config.getInt("haste 1", 50);
        hasteUnlock2 = config.getInt("haste 2", 100);
        if(hasteUnlock1 > -1) {
            addNotification(hasteUnlock1, Notification.ICON_PASSIVE, "Hasteful Gathering", messages.getMessage("notification_haste1"));
            if(hasteUnlock2 > -1) {
                addNotification(hasteUnlock2, Notification.ICON_PASSIVE, "Hasteful Gathering", messages.getMessage("notification_haste2"));
            }
        }
    }

    @Override
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {

        Player p = e.getPlayer();

        if(!isEnabled(e.getPlayer()))
            return;

        Block block = e.getBlock();
        float xp = ExpTables.getMiningExp(block);

        ItemStack item = null;
        if(xp > 0) {

            // no exp for player placed blocks
            if(PlayerPlacedListener.getInstance().collectPlayerPlaced(block))
                return;

            // no exp is given for silk touch if it changes the drops
            if(p.getInventory().getItemInMainHand().containsEnchantment(Enchantment.SILK_TOUCH) && SILK_TOUCH_BLOCKS.contains(block.getType())) {
                return;
            }

            RPGPlayer rp = getPlayer(p);
            int level = rp.getLevel(getId());

            if(block.getType() == Material.IRON_ORE || block.getType() == Material.GOLD_ORE) {

                if(rp.getSetting(AutosmeltCommand.SETTING_KEY) == null && autosmeltChance.isRandomChance(level)) {

                    e.setCancelled(true);
                    autoSmelt(block, p);
                }

            }

            rp.addExp(getId(), xp);
            if(block.getType() == Material.LAPIS_ORE) {
                rp.addExp(SkillType.ENCHANTING, 5);
            }

            // treasure
            if(treasureChance.isRandomChance(level)) {
                TreasureBag.getInstance().spawnTreasure(block, level);
            }

            // hastefull gathering
            if(hasteUnlock1 >= 0) {
                int tier = level >= hasteUnlock2 ? 2 : level >= hasteUnlock1 ? 1 : 0;
                if(tier > 0)
                    PotionEffectUtil.addPotionEffect(p, PotionEffectType.FAST_DIGGING, tier, 3, false, false);
            }

        }
    }

    private void autoSmelt(Block block, Player p) {

        Location center = block.getLocation().add(0.5, 0.5, 0.5);
        ItemStack newItem = new ItemStack( block.getType() == Material.IRON_ORE ? Material.IRON_INGOT : Material.GOLD_INGOT );

        int fortune = p.getInventory().getItemInMainHand().getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS);
        if(fortune > 0) {

            int bonus = CoreUtils.random.nextInt(fortune+2) - 2;
            if(bonus > 0)
                newItem.setAmount(bonus+1);

        }
        block.setType(Material.AIR);
        p.getWorld().spawnParticle(Particle.FLAME, center, 5, 0.0, 0.0, 0.0, 0.1);

        block.getWorld().dropItemNaturally(center, newItem);
    }

}
