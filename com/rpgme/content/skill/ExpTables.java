package com.rpgme.content.skill;

import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.EntityTypes;
import org.bukkit.Material;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class ExpTables implements Module {

    public static final float STAMINA_PER_SEC = 2.8f;
    public static final float STAMINA_JUMP_EXP = 0.8f;

    // cached for heavy use
    private static double expScale = 1.0;
    private static int targetLevel = 100;


    // settable from config
    public static void setRequirementMultiplier(double expScale) {
        ExpTables.expScale = expScale;
    }

    /**
     * Gets the exp required to reach level <code>level</code>
     * @param level
     * @return the exp at which you should be this level
     */
    public static int xpForLevel(int level) {
        //return (int) ( (Math.pow(lvl-1, 2.6) + ((lvl-1)*(35+lvl)) ) * expScale ) ;

        //return (int) ( (Math.pow(lvl, 2.6) + (lvl * (lvl+1) * 2) ) * 5 * expScale );

        if(level > 99) {
            // steeper curve for high levels
            return (int) (170000 + Math.pow(level-80, 3.6));
        }
        else {
            // regular curve
            return (int) ((Math.pow(level, 2.25) + (level * level * 1.6) ) * 4.6 * expScale) + 1;
        }
    }

    public static int getLevelAt(int exp) {
        int lvl = 1;
        int required = -1;
        while(exp > required) {required = (xpForLevel(++lvl)); }
        return lvl-1;
    }

    public static int getLevelAt(float f) {
        return getLevelAt((int)f);
    }


	/* combat */

    public static double getExpRewardForKilling(Entity killed) {
        double exp;
        if(EntityTypes.isPassive(killed)) {
            exp = 3.5;
        } else if(EntityTypes.isNetherMob(killed)) {
            exp = 7.5;
        } else if(EntityTypes.isBoss(killed)) {
            exp = 350;
        } else if(EntityTypes.isHostile(killed)) {
            exp = 5.5;
        } else {
            exp = 0;
        }
        return exp;
    }

	/* Mining */

    public static float getMiningExp(Block block) {
        Material mat = block.getType();
        switch(mat) {
            case NETHERRACK:
            case ENDER_STONE: return 0.1f;

            case SMOOTH_BRICK:
            case COBBLESTONE:
            case SANDSTONE:
            case RED_SANDSTONE:
            case STONE: return 1;

            case HARD_CLAY:
            case STAINED_CLAY:
            case MOSSY_COBBLESTONE: return 3;

            case OBSIDIAN:
            case QUARTZ_ORE:
            case COAL_ORE: return 5;

            case IRON_ORE: return 8;

            case LAPIS_ORE:
            case REDSTONE_ORE:
            case GLOWING_REDSTONE_ORE: return 10;

            case EMERALD_ORE:
            case GOLD_ORE: return 14;

            case DIAMOND_ORE: return 40;
            default: return 0;
        }
    }

    /* Landscaping */
    public static int getLandscapingExp(Material mat) {
        switch(mat) {
            case LEAVES:
            case LEAVES_2: return 1;
            case SNOW:
            case SAND:
            case DIRT: return 2;
            case GRASS:
            case SOUL_SAND:return 3;
            case GRAVEL:
            case SNOW_BLOCK: return 5;
            case CLAY: return 10;

            default: return 0;
        }
    }

    /* Woodcutting */
    @SuppressWarnings("deprecation")
    public static int getWoodcuttingExp(Block block) {
        byte data = block.getData();

        switch(block.getType()) {
            case LOG: {
                switch(data % 4) {
                    case 0: return 5; // oak
                    case 1: return 10; // birch
                    case 2: return 8; // spruce
                    case 3: return 6; // jungle
                }
            }
            case LOG_2: {
                switch(data % 2) {
                    case 0: return 12; // acacia
                    case 1: return 8; // dark oak
                }
            }
            case HUGE_MUSHROOM_1:
            case HUGE_MUSHROOM_2: return 5; // mushrooms

            default: return 0;
        }
    }

	/* Forging */

    public static final float FORGINGEXP_PER_DURABILITY = 2.5f;

    public static float getForgingExp(String material) {
        int amount = 2;
        switch(material.toLowerCase()) {

            case "diamond": amount *= 2;
            case "chainmail":
            case "gold": amount *= 2;
            case "leather":
            case "iron": amount *= 2;
            case "stone": amount *= 2;
            case "wood": break;
            default: return 0;

        }
        return amount;

    }

	/* Alchemy */

    public static int getAlchemyExp(Material ingredient) {
        switch(ingredient) {

            case NETHER_WARTS:
            case REDSTONE:
            case SUGAR:
            case SPIDER_EYE: return 7;
            case GLOWSTONE_DUST:
            case SULPHUR: return 9;
            case FERMENTED_SPIDER_EYE:
            case SPECKLED_MELON:
            case GOLDEN_CARROT: return 18;
            case BLAZE_POWDER:
            case MAGMA_CREAM: return 25;
            case GHAST_TEAR:
            case RABBIT_FOOT: return 40;
            case RAW_FISH: return 70; // pufferfish
            default: return 0;
        }

    }

	/* farming */

    // grow tree
    public static int getFarmingTreeExp(TreeType treeType) {
        switch(treeType) {

            case JUNGLE_BUSH: return 10;
            case TREE:
            case SWAMP: return 15;
            case BIRCH:
            case ACACIA:
            case REDWOOD:
            case SMALL_JUNGLE:
            case COCOA_TREE: return 20;
            case BROWN_MUSHROOM:
            case RED_MUSHROOM: return 25;
            case BIG_TREE: return 30;
            case TALL_BIRCH:
            case TALL_REDWOOD: return 40;
            case JUNGLE: return 60;
            case DARK_OAK:
            case MEGA_REDWOOD: return 80;
            default: return 0;
        }
    }

    // grow
    @Deprecated /** unused */
    public static int getFarmingGrowExp(Material crop) {
        switch(crop) {

            case SUGAR_CANE_BLOCK:
            case CROPS:
            case POTATO:
            case CARROT: return 4;
            case PUMPKIN:
            case MELON_BLOCK: return 6;
            case NETHER_WARTS:
            case COCOA: return 8;
            case PUMPKIN_STEM:
            case MELON_STEM: return 15;
            case CHORUS_PLANT:
            case CHORUS_FLOWER: return 12;
            default: return 0;
        }
    }
    // harvest
    public static float getFarmingHarvestExp(Material mat) {
        switch(mat) {
            case SUGAR_CANE_BLOCK:
            case CHORUS_PLANT:
            case CHORUS_FLOWER: return 5;
            case PUMPKIN_STEM:
            case MELON_STEM:
            case CROPS: return 6;
            case BEETROOT_BLOCK:
            case POTATO:
            case CARROT: return 8;
            case COCOA:
            case NETHER_WARTS: return 10;
            case PUMPKIN:
            case MELON_BLOCK: return 12;

            default: return 0;
        }
    }

	/* Taming */

    public static final double TAMING_EXP_RIDING_HORSE = 0.25;

    public static int getTameExp(EntityType type) {
        switch(type) {

            case HORSE: return 55;
            case WOLF:
            case OCELOT: return 35;

            default: return 0;
        }
    }

    public static int getBreedExp(EntityType type) {
        switch(type) {

            case CHICKEN:
            case COW:
            case PIG:
            case SHEEP:
            case RABBIT: return 10;
            case OCELOT:
            case WOLF: return 12;
            case HORSE:
            case IRON_GOLEM:
            case SNOWMAN: return 22;
            case VILLAGER: return 500; // for curing zombies

            default: return 0;
        }
    }


}
