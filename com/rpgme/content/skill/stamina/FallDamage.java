package com.rpgme.content.skill.stamina;

import com.rpgme.plugin.skill.Ability;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import com.rpgme.plugin.util.math.Scaler;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import java.util.List;


/**
 *
 */
public class FallDamage extends Ability<Stamina> {

    private final Scaler falldamageReduction = new Scaler(25, 15, 100, 80);

    public FallDamage(Stamina skill) {
        super(skill, "Delicate Landing", Stamina.ABILITY_FALL_DAMAGE);
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        super.createConfig(config, messages);
        // define the unlock notification
        messages.addValue("notification", "Reducing fall damage takes skill. And you've got it! Be careful though, jumping off a high cliff still gets you killed.");
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        super.onLoad(config, messages);
        String notification = messages.getMessage("notification");
        addNotification(falldamageReduction.minlvl, Notification.ICON_PASSIVE, getName(), notification);
    }

    @Override
    public void addCurrentStatistics(int forlevel, List<String> list) {
        if(forlevel >= falldamageReduction.minlvl)
            list.add("Fall Damage:-"+falldamageReduction.readableScale(forlevel) + '%');
    }


    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onFallDamage(EntityDamageEvent e) {
        if(e.getEntityType() == EntityType.PLAYER && e.getCause() == DamageCause.FALL) {

            if(e.getEntity().getFallDistance() > 25)
                return;

            Player p = (Player) e.getEntity();
            if(!isEnabled(p))
                return;

            int level = getLevel(p);
            if(level < falldamageReduction.minlvl)
                return;

            e.setDamage(e.getDamage() * ((100 - falldamageReduction.scale(level)) / 100));

        }
    }
}
