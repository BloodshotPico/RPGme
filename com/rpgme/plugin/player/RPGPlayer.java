package com.rpgme.plugin.player;

import com.rpgme.content.module.levelup.EffectsManager;
import com.rpgme.content.skill.ExpTables;
import com.rpgme.content.skill.SkillType;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.RPGPlayerProfile;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.event.RPGLevelupEvent;
import com.rpgme.plugin.event.SkillExpGainEvent;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.skill.Notification;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.MessageUtil;
import com.rpgme.plugin.util.PermissionChecker;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import java.text.MessageFormat;
import java.util.*;


public class RPGPlayer implements RPGPlayerProfile {

	public static final String SETTING_EXP_MESSAGE = "expMessage";
	public static final String SETTING_SCOREBOARD = "scoreboard";
	public static final String SETTING_STAMINA_MESSAGE = "showStamina";
	// exp values
	public static final String VALUE_BAR = "BAR";
	public static final String VALUE_STATS = "STATS";
	public static final String VALUE_GAIN = "GAIN";
	// scoreboard values
	public static final String VALUE_ALWAYS_ON = "ON";
	public static final String VALUE_ONLEVEL = "LEVEL";
	public static final String VALUE_ALWAYS_OFF = "OFF";
	// boolean values
	public static final String VALUE_TRUE = "true";
	public static final String VALUE_FALSE = "false";

	@SuppressWarnings("unchecked")
	// variables
	private List<RPGPlayer> partyMembers = Collections.emptyList();
	private IntMap<SortedSet<Notification>> notificationMap = new IntMap<>(1);

	private final UUID playerID;
	private final PlayerManager manager;
	private Player player;

	private final SkillSet skills;
	private final Map<String, String> settings = new HashMap<>(6);
	private SkillScoreboard scoreboard;

	// for cumalative exp message
	private transient float lastGains;
	private transient long atTime;
	private transient int forSkill;

	protected RPGPlayer(PlayerManager manager, UUID playerID, SkillSet skills) {
		this.manager = manager;
		this.playerID = playerID;
		this.skills = skills;
	}

	// config


	@Override
	public Map<String, String> getSettings() {
		return settings;
	}

	@Override
	public UUID getUniqueId() {
		return playerID;
	}

	public Player getPlayer() {
		if(player == null || !player.isValid())
			player = Bukkit.getPlayer(playerID);
		return player;
	}

	public RPGme getPlugin() {
		return manager.getPlugin();
	}

	@Override
	public SkillSet getSkillSet() {
		return skills;
	}

	// Parties
	public List<RPGPlayer> getPartyMembers() {
		return partyMembers;
	}

	protected void setPartyMembers(List<RPGPlayer> partyMembers) {
		this.partyMembers = partyMembers;
	}

	public boolean isInParty() {
		return getPartyMembers() != null;
	}

	public boolean isInPartyWith(RPGPlayer other) {
		return partyMembers.contains(other);
	}
	public boolean isInPartyWith(Player other) {
		for(RPGPlayer member : partyMembers) {
			if(member.getPlayer().equals(other))
				return true;
		}
		return false;
	}

	
	// notifications
	public void addNotification(int skill, Notification notification) {
		SortedSet<Notification> set = notificationMap.get(skill);
		if(set == null) {
			set = new TreeSet<>(new HashSet<Notification>(5));
		}
		set.add(notification);
		notificationMap.put(skill, set);
	}

	public SortedSet<Notification> getPersonalNotifications(int skill) {
		SortedSet<Notification> list = notificationMap.get(skill);
		return list != null ? list : Collections.emptySortedSet();
	}

	/**
	 * All settings are serialized and persistant between sessions.
	 * Values have to be configuration serializable (strings, string-list ect).
	 * Use value <b>null</b> to remove the entry 
	 */
	public void setSetting(String key, Object value) {
		if(value == null)
			settings.remove(key);
		else
			settings.put(key, value.toString());
	}

	public String getSetting(String key) {
		return settings.get(key);
	}

	public String getSetting(String key, String def) {
		String string = settings.get(key);
		return string != null ? string : def;
	}

	public String getExpSetting() {
		return getSetting(SETTING_EXP_MESSAGE, VALUE_BAR);
	}

	public String getScoreboardSetting() {
		return getSetting(SETTING_SCOREBOARD, VALUE_ONLEVEL);
	}

	public Boolean showStamina() {
		return Boolean.valueOf(getSetting(SETTING_STAMINA_MESSAGE, VALUE_FALSE));
	}

    public PlayerManager getManager() {
        return manager;
    }

    // Exp
	public float getExp(int skill) {
		return skills.getExp(skill);
	}

	public int getLevel(int skill) {
		return skills.getLevel(skill);
	}

	public float getExp(Skill skill) {
		return skills.getExp(skill.getId());
	}

	public int getLevel(Skill skill) {
		return skills.getLevel(skill.getId());
	}

	/**
	 * Adds exp to this player the 'regular' way. This will check config, modifiers, party members and more before applying the exp.
	 * This method should be used by skills for rewarding various in-game actions.
	 * In the process of this method a {@link SkillExpGainEvent} will be fired which can be cancelled or changed 
	 * @param skill
	 * @param amount
	 * @see #addTrueExp(Skill, float)
	 */
	public void addExp(int skill, float amount) {
		Skill module = getPlugin().getSkillManager().getById(skill);
		addExp(module, amount);
	}

	public void addExp(Skill skill, float amount) {
		Player p = getPlayer();

		// checks
		if(!PermissionChecker.isEnabled(p, skill) || (p.getGameMode() != GameMode.SURVIVAL && manager.isSurvivalOnly()))
			return;

		// multipliers
		amount = (float) (amount * manager.getGainMultiplier() * skill.getExpMultiplier() * PluginIntegration.getInstance().getExpMultiplier(p, p.getLocation()));

		if(amount < 0.1f)
			return;

		// skill enchantments
		// should move to skill enchant module
//		if(getPlugin().getConfig().getBoolean("Skill Enchants.enabled")) {
//			ItemStack[] equipment = p.getInventory().getContents();
//			int extra = 0;
//			for(ItemStack item : equipment) {
//
//				if(!SkillEnchantment.isEnchantedItem(item))
//					continue;
//
//				SkillEnchantment enchant = new SkillEnchantment(item);
//				if(enchant.getType() == skill.getDurability()) {
//					extra += enchant.getBoost();
//				}
//			}
//			if(extra > 0)
//				amount *= SkillEnchantment.toMultiplier(extra);
//		}

		// event
		SkillExpGainEvent event = new SkillExpGainEvent(this, skill, amount);
		getPlayer().getServer().getPluginManager().callEvent(event);

		if(event.isCancelled())
			return;
		amount = event.getExp();

		// register to this player
		addTrueExp(skill, amount);
	}

	/**
	 * Adds exp directly to this player. This does not check for exp modifiers, gamemode or partymembers.
	 * The player recieves an Exp gain message, but no SkillExpGainEvent is fired
	 * @param skill The skill to increase
	 * @param amount The amount of exp. Values <= 0f will be ignored.
	 */
	public void addTrueExp(Skill skill, float amount) {
		if(amount <= 0f)
			return;

		if(amount > 0.1f && (skill.getId() != SkillType.STAMINA || showStamina()))
			sendExpgainMessage(skill, amount);

		getSkillSet().addExp(skill.getId(), amount);
	}
	/**
	 * Overrides an exp value. This is an advanced/admin method
	 * @param skill (enum)
	 * @param newamount
	 * @return the previous exp for the given skill
	 */
	public float setExp(int skill, float newamount) {
		float current = getExp(skill);
		getSkillSet().setExp(skill, newamount);
		getSkillSet().recalculateLevel(skill);

		return current;
	}

	protected void onLevelup(int skill) {
		SkillSet.ExpEntry current = skills.skillMap.get(skill);
		current.setLevel(current.getLevel()+1);

		Player p = getPlayer();
		if(p == null)
			return;

		Skill module = getPlugin().getSkillManager().getById(skill);
		p.sendMessage(getPlugin().getMessages().getMessage("exp_levelup", module.getDisplayName(), current.getLevel()));
		GameSound.play(Sound.ENTITY_PLAYER_LEVELUP, p.getLocation());

		module.sendNotifications(this, current.getLevel());
		updateScoreboard();

        String effectSetting = getSetting(EffectsManager.SETTING_KEY, "Default");
        EffectsManager.startByName(getPlayer(), effectSetting);

		Event event = new RPGLevelupEvent(this, module, current.getLevel());
		Bukkit.getPluginManager().callEvent(event);
	}

	// Other

	/**
	 * Sends the exp gain message to the player depending on his current settings
	 */
	public void sendExpgainMessage(Skill skill, float increase) {
		int id = skill.getId();

		Player p = getPlayer();
		String skillDisplayname = getPlugin().getSkillManager().colorDark() + getLevel(id) + " " + getPlugin().getSkillManager().colorLight() + skill.getDisplayName();

        switch(getExpSetting()) {
		case VALUE_BAR:
			MessageUtil.sendToActionBar(p, buildExpBar(skillDisplayname, getLevel(id), getExp(id)+increase));
			break;

		case VALUE_GAIN:
			String total = getExpGainToShow(id, increase);
            MessageUtil.sendToActionBar(player,
					getPlugin().getMessages().getMessage("exp_gain", total, skill.getDisplayName()));
			break;

		case VALUE_STATS:
			int current = (int) (getExp(id)+increase);
            MessageUtil.sendToActionBar(player,
					getPlugin().getMessages().getMessage("exp_stats", current, ExpTables.xpForLevel(getLevel(id)+1) - current, skillDisplayname));
			break;
		}
	}

	/**
	 * For cummolative gains
	 */
	private String getExpGainToShow(int forSkill, float increase) {
		if(lastGains == 0f || atTime + 3000 < System.currentTimeMillis() || forSkill != this.forSkill)
			lastGains = 0f;

		atTime = System.currentTimeMillis();
		this.forSkill = forSkill;
		lastGains += increase;
		return lastGains == (int) lastGains ? String.valueOf((int) lastGains) : String.format("%.1f", lastGains);
	}

	/**
	 * Activate the players scoreboard depending on personal current settings
	 */
	public void updateScoreboard() {
		if(!manager.isScoreboardEnabled())
			return;

		String setting = getScoreboardSetting();

		if(setting.equalsIgnoreCase(VALUE_ALWAYS_ON) || setting.equalsIgnoreCase(VALUE_ONLEVEL)) {

			if(scoreboard == null) {
				scoreboard = new SkillScoreboard(this);
			}

			scoreboard.build();

			scoreboard.cancelScheduledRemoval();
			if(setting.equalsIgnoreCase(VALUE_ONLEVEL)) {
				scoreboard.scheduleRemoval(12);
			}

		} else if(setting.equalsIgnoreCase(VALUE_ALWAYS_OFF)) {
			getPlayer().setScoreboard(Bukkit.getServer().getScoreboardManager().getMainScoreboard());
			scoreboard = null;
		}
	}

	@Override
	public int hashCode() {
		return playerID.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;

		if(obj.getClass() == UUID.class) {
			return obj.equals(playerID);
		}

		if (!(obj instanceof RPGPlayer))
			return false;
		RPGPlayer other = (RPGPlayer) obj;
		if (playerID == null) {
			if (other.playerID != null)
				return false;
		} else if (!playerID.equals(other.playerID))
			return false;
		return true;
	}

    public String buildExpBar(String skillDisplayname, int currentLvl, float newexp) {
        StringBuilder builder = new StringBuilder();

        int start = ExpTables.xpForLevel(currentLvl);
        int end = ExpTables.xpForLevel(currentLvl+1);

        String org = getPlugin().getMessages().getMessage("exp_bar");
        String cha = getPlugin().getMessages().getMessage("exp_bar_char");

        builder.append(org.substring(0, org.indexOf(cha))).append(ChatColor.GREEN.toString());

        int count = StringUtils.countMatches(org, cha);

        int green = Math.max(0, (count * ((int)Math.ceil(newexp) - start) / (end - start)  ));

        builder.append(StringUtils.repeat(cha, green)).append(ChatColor.GRAY.toString()).append(StringUtils.repeat(cha, count - green));

        builder.append(org.substring(org.indexOf(cha) + count));

        return MessageFormat.format(builder.toString(), skillDisplayname);
    }


}
