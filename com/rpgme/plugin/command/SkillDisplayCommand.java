package com.rpgme.plugin.command;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.util.config.BundleSection;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Locale;

/**
 * Shows the text of /<skill> commands.
 * However it does not actually do anything itself. It passes this work to the {@link Skill} implementations of {@link com.rpgme.plugin.skill.BaseSkill}.
 */
public class SkillDisplayCommand extends CoreCommand {

    public SkillDisplayCommand(RPGme plugin) {
        super(plugin, "skill");
        setConsoleAllowed(false);

        setDescription("Displays a personal overview of a particular skill");
        setCommandHelp(new CommandHelp("/<skill>", "Show a personal and interactive overview of a skill in your chat box"));
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        final List<String> aliases = Lists.newArrayList();
        for(Skill skill : plugin.getSkillManager().getEnabledSkills()) {

            aliases.add(skill.getName().toLowerCase(Locale.ENGLISH));
            if(skill.hasDisplayName()) {
                aliases.add(skill.getDisplayName().toLowerCase(Locale.ENGLISH));
            }
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                plugin.getCommandManager().unregister(SkillDisplayCommand.this);
                setAliases(aliases);
                register();
            }
        }.runTask(plugin);
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {

        Player p = (Player) sender;
        String skillName;

        if(alias.equalsIgnoreCase("skill")) {

            if(flags.isEmpty()) {

                new SkillOverviewCommand.SkillDisplayBuilder(plugin, p);
                return;
            }
            skillName = flags.get(0);

        } else {
            skillName = alias;
        }

        Skill skill = plugin.getSkillManager().getByName(skillName);
        if(skill == null || !skill.isEnabled(p)) {
            error(sender, "Skill '"+skillName+"' not recognized.");
            StringBuilder sb = new StringBuilder("Available kills: ");
            for(Skill s : plugin.getSkillManager().getEnabledSkills(p)) {
                sb.append(s.getDisplayName().toLowerCase()).append(", ");
            }
            sb.delete(sb.length() -2, sb.length());
            p.sendMessage(sb.toString());
            return;
        }

        skill.showCommandText(p);
    }

    @Override
    public List<String> getTabComplete(CommandSender sender, String label,
                                       List<String> args) {
        return null;
    }

}
