package com.rpgme.plugin.command;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class CoreCommand extends Command implements Module, CommandExecutor {
	
	private static final String NO_CONSOLE = "[Error] Command cannot be executed from console.";
	
	protected final RPGme plugin;
		
	private final String[] permissions;
    private CommandHelp commandHelp;
	private boolean consoleAllowed = true, showHelp = true, checkPermissions = true;

	protected CoreCommand(RPGme plugin, String command, String... permissions) {
		super(command);
		this.plugin = plugin;
		this.permissions = permissions.length > 0 ? permissions : null;
	}

	public boolean hasPermission(CommandSender sender) {
		return hasPermission(sender, true);
	}

	public boolean hasPermission(CommandSender sender, boolean notify) {
		if(permissions == null) 
			return true;

		for(String perm : permissions) {
			if(sender.hasPermission(perm))
				return true;
		}
		if(notify) {
			sender.sendMessage(plugin.getMessages().getMessage("err_nopermission"));
		}
		return false;
	}

	public boolean hasPermission(CommandSender sender, String perm) {
		return hasPermission(sender, perm, true);
	}
	public boolean hasPermission(CommandSender sender, String perm, boolean notify) {
		if(sender.hasPermission(perm)) {
			return true;
		}
		if(notify)
			sender.sendMessage(plugin.getMessages().getMessage("err_nopermission"));
		return false;
	}

	public boolean consoleCheck(CommandSender sender) {
		if(!(sender instanceof Player)) {
			sender.sendMessage(NO_CONSOLE);
			return false;
		}
		return true;
	}

	public void error(CommandSender sender, String error) {
		sender.sendMessage(ChatColor.RED + error);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		// Checking sender:
		if(!consoleAllowed && !consoleCheck(sender)) {
			return true;
		}

		// Checking permission:
		if(checkPermissions && !hasPermission(sender))
			return true;
		
		// filling flags (args)
		List<String> list = new ArrayList<String>(Arrays.asList(args));

		// print help
		if(showHelp && (list.contains("help") || list.contains("?"))) {
			printCommandHelp(sender);
		}
		else {
			// or execute
			execute(sender, label, list);
		}
		return true;
	}
	
	
	public abstract void execute(CommandSender sender, String label, List<String> args);

	@Override
	public boolean execute(CommandSender sender, String commandLabel, String[] args) {
		return onCommand(sender, this, commandLabel, args);
	}

	public abstract List<String> getTabComplete(CommandSender sender, String label, List<String> args);

	public void setAliases(String... aliases) {
		setAliases(Arrays.asList(aliases));
	}

    public CommandHelp getCommandHelp() {
        if(commandHelp == null)
            commandHelp = new CommandHelp();

        return commandHelp;
    }

    public CoreCommand setCommandHelp(CommandHelp commandHelp) {
        if(commandHelp == null)
            commandHelp = new CommandHelp();

        this.commandHelp = commandHelp;
        super.setUsage(String.join(", ", commandHelp.getUsages()));

        return this;
    }

    public void printCommandHelp(CommandSender sender) {
        plugin.getCommandManager().printCommandHelp(this, sender);
    }

	public String[] getPermissions() {
		return permissions;
	}

	public RPGme getPlugin() {
		return plugin;
	}

	public boolean isConsoleAllowed() {
		return consoleAllowed;
	}

	public void setConsoleAllowed(boolean consoleAllowed) {
		this.consoleAllowed = consoleAllowed;
	}
	
	public void setShowHelp(boolean showHelp) {
		this.showHelp = showHelp;
	}
	
	public void setPermissionCheck(boolean checkPermissions) {
		this.checkPermissions = checkPermissions;
	}
	
	public List<String> getOnlinePlayers() {
		List<String> list = Lists.newArrayList();
		for(Player p : plugin.getServer().getOnlinePlayers())
			list.add(p.getName());
		return list;
	}

	public void register() {
		plugin.getCommandManager().register(this);
	}

}
