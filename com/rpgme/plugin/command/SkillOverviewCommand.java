package com.rpgme.plugin.command;

import com.rpgme.content.skill.ExpTables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.manager.SkillManager;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.PermissionChecker;
import com.rpgme.plugin.util.Symbol;
import com.rpgme.plugin.util.menu.ConfigurationMenu;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import net.md_5.bungee.api.chat.HoverEvent;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.List;

/**
 * Builds the command text for /skills.
 * It shows each of your skills with their levels. Each one their own skill overview command.
 */
public class SkillOverviewCommand extends CoreCommand {

	public SkillOverviewCommand(RPGme plugin) {
		super(plugin, "skills");
		setConsoleAllowed(false);
		setAliases("stats", "levels", "settings");
		setDescription("Shows you an interactive overview of your skills in chat");
        setCommandHelp(new CommandHelp.Builder()
                .addUsage("/stats", "See all your skills with their current levels. Click on a skill to show it's detailed overview")
                .addUsage("/settings", "Open your RPGme settings menu. Here you can toggle effects and display options.")
                .build());
	}

	@Override
	public List<String> getTabComplete(CommandSender sender, String label,
			List<String> args) {
		return getOnlinePlayers();
	}

	@Override
	public void execute(CommandSender sender, String alias, List<String> flags) {

		Player p = (Player) sender;
		if(!PermissionChecker.isLoaded(p)) {
			error(p, "Oops. Something went wrong. It appears your RPGme profile is not loaded.");
			return;
		}

		if(alias.equalsIgnoreCase("settings") || flags.remove("settings")) {

			new ConfigurationMenu(plugin, p).openGUI();

		} else {

			if(!flags.isEmpty() && sender.hasPermission("rpgme.stats.other")) {

				Player target = plugin.getServer().getPlayer(flags.get(0));
				if(target != null) {
					sender.sendMessage(ChatColor.GREEN + "Skill statistics for player: "+ ChatColor.DARK_GREEN + target.getName());
					new SkillDisplayBuilder(plugin, target, p);
				} else {
					error(sender, "Player '"+flags.get(0) + "' not found.");
				}
			} 
			else {
				new SkillDisplayBuilder(plugin, p);
			}
		}


	}


	public static class SkillDisplayBuilder {

		final RPGme plugin;
		final RPGPlayer player;
		final Player showTo;

        String colorPrimary, colorPrimaryDark, colorAccent;

		SkillDisplayBuilder(RPGme plugin, Player p) {
			this(plugin, p, p);
		}

		SkillDisplayBuilder(RPGme plugin, Player target, Player showTo) {
			this.plugin = plugin;
			this.player = plugin.getPlayer(target);
			this.showTo = showTo;

            SkillManager manager = plugin.getSkillManager();
            colorPrimary = manager.colorLight();
            colorPrimaryDark = manager.colorDark();
            colorAccent = manager.colorAccent();

			buildMessage();
		}

		private void buildMessage() {
            // top line
			ComponentBuilder builder = new ComponentBuilder(Symbol.lineTop).color(net.md_5.bungee.api.ChatColor.GRAY).strikethrough(true).append("\n ", FormatRetention.NONE);
            // combat level icon
			if(plugin.getConfig().getBoolean("Show Combat Level")) {
				int combat = player.getSkillSet().getCombatLevel();
				builder.append(StringUtils.repeat(' ', 4)).append(Symbol.DISPLAY_COMBAT).bold(true).append(" "+ combat)
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
						new ComponentBuilder(plugin.getMessages().getMessage("skills_combat")).color(net.md_5.bungee.api.ChatColor.GRAY).append(" " + combat).color(net.md_5.bungee.api.ChatColor.RED).create()))
						.append(String.valueOf(combat), FormatRetention.EVENTS).color(net.md_5.bungee.api.ChatColor.RED);

			}

			builder.append("\n  ").retain(FormatRetention.NONE);

            // all of the skills, with hover events
			SkillManager skillManager = plugin.getSkillManager();
			int count = 0; 
			Collection<Skill> skills = skillManager.getEnabledSkills(player);
			for(Skill skill : skills) {
				int currentXP = (int) player.getExp(skill);
				int level = player.getLevel(skill);
				int requiredXP = ExpTables.xpForLevel(level+1) - currentXP;

				builder.append("  " + skillManager.colorLight() + String.valueOf(level) + ' ' + skillManager.colorDark() + skill.getDisplayName() + ' ')
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, 
						skillHoverText(skill.getDisplayName(), level, currentXP, requiredXP)));
				// only register click command for your own stats
				if(player.getPlayer() == showTo)
					builder.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/skill "+skill.getName().toLowerCase()));

				if(count == skills.size() -1 || ++count % 3 == 0) {
					builder.append("\n  ", FormatRetention.NONE);
				} else {
					builder.append(" |", FormatRetention.NONE).color(net.md_5.bungee.api.ChatColor.DARK_GRAY);
				}

			}

            // total level
			int total = player.getSkillSet().getTotalLevel();
			builder.append("\n   ").append(plugin.getMessages().getMessage("skills_total")).color(net.md_5.bungee.api.ChatColor.BLUE).append(" ").append(String.valueOf(total)).color(net.md_5.bungee.api.ChatColor.WHITE)
			.append(StringUtils.repeat(' ', 10)).append(Symbol.configButton).italic(true)
			.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/settings"))
			.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(plugin.getMessages().getMessage("ui_settings")).color(net.md_5.bungee.api.ChatColor.WHITE).append("\n" + plugin.getMessages().getMessage("ui_settings_clickme")).color(net.md_5.bungee.api.ChatColor.GRAY).italic(true).create()));

            // if the player is a 'beginner', show a tip
            if(player.getSkillSet().getTotalLevel() <= 30) {
                builder.append(StringUtils.repeat(' ', 16)).append(plugin.getMessages().getMessage("skills_hoverme")).color(net.md_5.bungee.api.ChatColor.GRAY);
            }

            builder.append("\n", FormatRetention.NONE).append(Symbol.lineBottom);

			showTo.spigot().sendMessage(builder.create());
		}

		private BaseComponent[] skillHoverText(String title, int level, int current, int required) {

			int maxlength = String.valueOf(required).length() + 11;
			int spaces = maxlength - title.length() / 2;
			title = StringUtils.repeat(' ', spaces)+title;

            ComponentBuilder builder = new ComponentBuilder(ChatColor.BLUE.toString() + ChatColor.BOLD + title + "\n" +
                    ChatColor.WHITE + plugin.getMessages().getMessage("ui_currentlevel") + " " + colorPrimaryDark + level + "\n" +
                    ChatColor.WHITE + "Exp: " + colorPrimary + current + "\n" +
                    ChatColor.WHITE + plugin.getMessages().getMessage("ui_nextlevel") + " " + colorAccent + required);

            if(player.getPlayer() == showTo) {
                builder.append("\n" + ChatColor.GRAY + ChatColor.ITALIC + plugin.getMessages().getMessage("ui_clickme"));
            }
            return builder.create();
//			return new ComponentBuilder(title).color(net.md_5.bungee.api.ChatColor.BLUE).bold(true).append("\n")
//					.append(plugin.getMessages().getMessage("ui_currentlevel")).bold(false).color(net.md_5.bungee.api.ChatColor.WHITE).append(" ", FormatRetention.NONE).append(plugin.getSkillManager().colorDark()).append(String.valueOf(level)).append("\n")
//					.append("Exp: ").color(net.md_5.bungee.api.ChatColor.WHITE).append(plugin.getSkillManager().colorLight(), FormatRetention.NONE).append(String.valueOf(current)).append("\n")
//					.append(plugin.getMessages().getMessage("ui_nextlevel")).color(net.md_5.bungee.api.ChatColor.WHITE).append(" ").append(required + " exp").color(net.md_5.bungee.api.ChatColor.AQUA)
//					.append("\n").append(player == showTo ? plugin.getMessages().getMessage("ui_clickme") : " ").color(net.md_5.bungee.api.ChatColor.GRAY).italic(true).create();

		}

	}


}
