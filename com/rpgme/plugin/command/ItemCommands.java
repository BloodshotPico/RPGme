package com.rpgme.plugin.command;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.treasure.TreasureBag;
import com.rpgme.plugin.treasure.TreasureChest;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.RPGItems;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 26/02/2017.
 */
public class ItemCommands extends CoreCommand {

    private static final String PERMISSION = "rpgme.command.admin";

    private static final String USAGE_EXPTOMB = "/exptomb <exp> (minlevel) (skill) (player) (amount)";
    private static final String USAGE_ENCHANT = "/skillenchant <skill> <boost> (player)";
    private static final String USAGE_TREASURE = "/treasure (level) (player) (amount)";
    private static final String USAGE_SKILLGEM = "/skillgem (player)";

    public ItemCommands(RPGme plugin) {
        super(plugin, "rpgitems", PERMISSION);
        setConsoleAllowed(true);

        setAliases("exptomb", "treasure", "treasurechest");
        setDescription("Command to spawn custom rpgme items: exp tombs that grant exp to players and treasure chests to roll for loot.");
        setCommandHelp(new CommandHelp.Builder()
        .addUsage(USAGE_EXPTOMB, "Create an exp tomb, optionally specifying a minimum required level, " +
                "a target player, an amount and comma separated list of skills to choose from.")
        .addUsage(USAGE_TREASURE, "Spawn a treasure chest, optionally specifying a level (0-100), player and count.")
        .build());
    }

    @Override
    public void execute(CommandSender sender, String alias, List<String> flags) {
        switch (alias) {
            case "exptomb":
                handleExptomb(sender, flags);
                break;
            case "treasure":
            case "treasurechest":
                handleTreasure(sender, flags);
                break;
            default:
                plugin.getCommandManager().printCommandHelp(this, sender);
        }
    }

    private void handleExptomb(CommandSender sender, List<String> flags) {
        int exp = -1;
        int minlvl = -1;
        int count = -1;
        Player target = null;
        List<Skill> restrictedSkills = new ArrayList<>();

        for(String arg : flags) {

            if(StringUtils.isNumeric(arg)) {

                int num = Integer.parseInt(arg);
                if(exp < 0) {
                    exp = num;
                } else if(minlvl < 0) {
                    minlvl = num;
                } else if(count < 0) {
                    count = num;
                } else {
                    error(sender, "Too many numeric arguments. " + USAGE_EXPTOMB);
                }
                continue;
            }

            Player player = plugin.getServer().getPlayer(arg);
            if(player != null) {
                target = player;
                continue;
            }

            String[] split = arg.split(",");
            for(String s : split) {
                Skill skill = plugin.getSkillManager().getByName(s);
                if(skill != null) {
                    restrictedSkills.add(skill);
                }
            }
        }

        if(exp < 0) {
            error(sender, "Not enough arguments: " + USAGE_EXPTOMB);
            return;
        }
        if(minlvl < 0)
            minlvl = 1;
        if(count < 0)
            count = 1;
        if(target == null) {
            if(!consoleCheck(sender)) {
                error(sender, "Please specify a player");
                return;
            }
            target = (Player) sender;
        }
        String restriction = "";
        if(restrictedSkills.isEmpty()) {
            restrictedSkills.addAll(plugin.getSkillManager().getEnabledSkills(target));
            restriction = "for any skill";
        } else {
            restriction = "restricted to ";
            int restrictionCount = restrictedSkills.size();
            for(int i = 0; i < restrictionCount; i++) {
                restriction += restrictedSkills.get(i).getDisplayName();
                if(i < restrictionCount -1) {
                    restriction += ", ";
                }
            }
        }

        ItemStack item = RPGItems.createExpTomb(exp, minlvl, restrictedSkills);
        item.setAmount(count);
        ItemUtils.give(target, item);

        GameSound.play(Sound.ENTITY_ITEM_PICKUP, target);
        sender.sendMessage(String.format(ChatColor.YELLOW + "Given %d exp tomb(s) worth %d exp to %s. Required level is %d, %s",
                count, exp, target.getName(), minlvl, restriction));
    }

    private void handleTreasure(CommandSender sender, List<String> flags) {
        Player target = null;
        int level = -1;
        int count = -1;

        for(String arg : flags) {

            if(StringUtils.isNumeric(arg)) {
                int num = Integer.parseInt(arg);
                if(level < 0) {
                    level = num;
                } else if(count < 0) {
                    count = num;
                } else {
                    error(sender, "Too many numeric arguments. " + USAGE_TREASURE);
                }
            }

            Player player = plugin.getServer().getPlayer(arg);
            if(player != null) {
                target = player;
            }
        }

        if(target == null) {
            if(!consoleCheck(sender)) {
                error(sender, "Please specify a player");
                return;
            }
            target = (Player) sender;
        }
        if(level < 0) {
            level = plugin.getPlayer(target).getSkillSet().getAverageLevel();
        }
        if(count < 0) {
            count = 1;
        }

        TreasureBag treasureManager = plugin.getModule(RPGme.MODULE_TREASURES);
        for(int i = 0; i < count; i++) {
            ItemStack item = treasureManager.rollTreasureChest(level);
            ItemUtils.give(target, item);
        }
        sender.sendMessage(String.format(ChatColor.YELLOW + "Given %d treasure chest(s) of tier %s to %s", count, TreasureChest.toRomanTier(level), target.getName()));
    }

//    private void handleSkillgem(CommandSender sender, List<String> flags) {
//        Player player = null;
//        boolean amount = true;
//        Iterator var6 = flags.iterator();
//
//        while(var6.hasNext()) {
//            String s = (String)var6.next();
//            Player selected = ((RPGmePremium)this.plugin).getServer().getPlayer(s);
//            if(selected != null) {
//                player = selected;
//            } else if(StringUtils.isNumeric(s)) {
//                int amount1 = Integer.parseInt(s);
//            }
//        }
//
//        if(player == null) {
//            if(!this.consoleCheck(sender)) {
//                return;
//            }
//
//            player = (Player)sender;
//        }
//
//        ItemUtil.give(player, new ItemStack[]{((RPGmePremium)this.plugin).createSkillGem()});
//        GameSound.play(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, player.getLocation(), 1.2F, 0.7F);
//        if(player != sender) {
//            sender.sendMessage(ChatColor.GREEN + " Skill Gem given to " + player.getName());
//        }
//
//    }
//
//
//    private void handleSkillenchantCommand(CommandSender sender, List<String> flags) {
//        if(flags.size() < 2) {
//            this.error(sender, "Not enough arguments.\n/skillenchant <skill> <boost> (player)");
//        } else {
//            SkillType skill = SkillType.getByAlias((String)flags.get(0));
//            if(skill == null) {
//                this.error(sender, "Skill " + (String)flags.get(0) + " not recognized\n" + "/skillenchant <skill> <boost> (player)");
//            } else {
//                int boost;
//                try {
//                    boost = Maths.clamp(Integer.parseInt((String)flags.get(1)), 1, 100);
//                } catch (NumberFormatException var7) {
//                    this.error(sender, "Argument \'" + (String)flags.get(1) + "\' should be a number (boost percentage)\n" + "/skillenchant <skill> <boost> (player)");
//                    return;
//                }
//
//                Player target;
//                if(flags.size() > 2) {
//                    target = ((RPGmePremium)this.plugin).getServer().getPlayer((String)flags.get(2));
//                    if(target == null) {
//                        this.error(sender, "Player \'" + (String)flags.get(2) + "\' not found.\n" + "/skillenchant <skill> <boost> (player)");
//                        return;
//                    }
//                } else {
//                    if(!this.consoleCheck(sender)) {
//                        this.error(sender, "Please specifiy a player to give the exp tomb to.\n/skillenchant <skill> <boost> (player)");
//                        return;
//                    }
//
//                    target = (Player)sender;
//                }
//
//                ItemStack book = (new SkillEnchantment(skill, boost)).getEnchantedBook();
//                ItemUtil.give(target, new ItemStack[]{book});
//                GameSound.play(Sound.ENTITY_ITEM_PICKUP, target);
//                if(!sender.equals(target)) {
//                    sender.sendMessage("Skillenchantment book (" + skill.readableName() + ' ' + boost + "%) given to " + target.getName());
//                }
//
//            }
//        }
//    }
//
//    public void addCommandHelp(CommandSender sender, List<String> list) {
//        if(this.hasPermission(sender, false)) {
//            list.add("/exptomb <exp> <minlevel/skill> (player) (amount):Give an exp tomb to you or a player");
//            list.add("/skillenchant <skill> <boost> (player):Give an skill-enchantment book to you or a player");
//            list.add("/treasure <level> (player):Give an treasure chest to you or a player");
//        }
//
//    }
//
//    public List<String> getTabComplete(CommandSender sender, String label, List<String> flags) {
//        switch(label.hashCode()) {
//            case -1308910355:
//                if(label.equals("exptomb")) {
//                    return this.getOnlinePlayers();
//                }
//                break;
//            case -493914904:
//                if(label.equals("skillenchant")) {
//                    return flags.size() < 2?SkillType.getEnabledNames():this.getOnlinePlayers();
//                }
//                break;
//            case 599005618:
//                if(label.equals("treasurechest")) {
//                    return this.getOnlinePlayers();
//                }
//                break;
//            case 1383408303:
//                if(label.equals("treasure")) {
//                    return this.getOnlinePlayers();
//                }
//        }
//
//        return null;
//    }


    @Override
    public List<String> getTabComplete(CommandSender sender, String label, List<String> args) {
        return null;
    }
}
