package com.rpgme.plugin.blockmeta;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.blockmeta.data.BooleanStorageLayer;
import com.rpgme.plugin.blockmeta.data.StorageLayer;
import com.rpgme.plugin.blockmeta.data.StringStorageLayer;
import com.rpgme.plugin.util.PermissionChecker;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.YamlFile;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class BlockDataManager implements Module {
	
	private final Map<String, Class<?>> layerMap = new HashMap<>();
	private final Map<World, List<StorageLayer<?>>> dataMap = new HashMap<>();

	final RPGme plugin;

    private PlayerPlacedListener playerPlacedListener;
    private boolean hasLoadedData = false;
		
	public BlockDataManager(RPGme plugin) {
		this.plugin = plugin;
	}

	@Override
	public void onEnable() {
		playerPlacedListener = new PlayerPlacedListener(this);
        playerPlacedListener.registerListeners();
	}

    @Override
    public void onDisable() {
        saveAll();
    }

    public PlayerPlacedListener getPlayerPlacedListener() {
        return playerPlacedListener;
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        reloadData();
        hasLoadedData = true;
    }

    protected void reloadData() {
        if(hasLoadedData) {
            // reload
            saveAll();
            dataMap.clear();
        }

        plugin.getServer().getWorlds().forEach(this::addWorld);

        for(Entry<World, List<StorageLayer<?>>> entry : dataMap.entrySet()) {
            YamlFile file = new YamlFile(getFile(entry.getKey()));
            file.read();

            for(StorageLayer<?> storageLayer : entry.getValue()) {
                if(storageLayer.size() == 0) {
                    storageLayer.readFromFile(file.data);
                }
            }
        }
    }

    protected void addWorld(World world) {
        if(!PermissionChecker.isDisabledWorld(world)) {

            List<StorageLayer<?>> list = dataMap.get(world);
            if(list == null) {
                list = new ArrayList<>(layerMap.size());
                dataMap.put(world, list);
            }

            for(Entry<String, Class<?>> entry : layerMap.entrySet()) {

                if(containsName(list, entry.getKey())) {
                    continue;
                }
                StorageLayer<?> layer = entry.getValue() == Boolean.class ? new BooleanStorageLayer(entry.getKey()) : new StringStorageLayer(entry.getKey());
                list.add(layer);
            }
        }
    }

    public void addEmptyLayer(String key, Class<?> type) {
        layerMap.put(key, type);
        if(hasLoadedData) {
            reloadData();
        }
    }

    protected boolean containsName(List<StorageLayer<?>> list, String name) {
		for(StorageLayer<?> layer : list) {
			if(layer.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

	public StorageLayer<?> getLayer(World world, String key) {
		List<StorageLayer<?>> list = dataMap.get(world);
		if(list == null) return null;
		
		for(StorageLayer<?> store : list) {
			if(store.getName().equals(key))
				return store;
		}
		return null;
	}
	
	public boolean set(Block block, String layerName, Object value) {
		StorageLayer<?> layer = getLayer(block.getWorld(), layerName);
		if(layer == null) throw new IllegalArgumentException("No layer for name '" + layerName + "'");
		
		if(layer.getClass() == BooleanStorageLayer.class) {
			((BooleanStorageLayer)layer).setData(block, (Boolean)value);
		} else if(layer.getClass() == StringStorageLayer.class) {
			((StringStorageLayer)layer).setData(block, value.toString());
		} else {
            return false;
		}
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	public <T> T collect(Block block, String layerName, Class<T> valueType) {
		StorageLayer<?> layer = getLayer(block.getWorld(), layerName);
		if(layer == null) return null;
		
		try{
			T value = (T) layer.getData(block);
			layer.setData(block, null);
			return value;
		} catch(ClassCastException e) {
            return null;
		}
	}
		
	public boolean remove(Block block, String layerName) {
		StorageLayer<?> layer = getLayer(block.getWorld(), layerName);
		if(layer != null) {
			layer.setData(block, null);
			return true;
		}
		return false;
	}

	protected void saveAll() {
		for(Entry<World, List<StorageLayer<?>>> entry : dataMap.entrySet()) {
			
			YamlFile file = new YamlFile(getFile(entry.getKey()));
			file.clear();
			
			for(StorageLayer<?> storageLayer : entry.getValue()) {
				storageLayer.writeToFile(file.data);
			}
			
			file.write();
			
		}
	}

    protected static File getFile(World world) {
		return new File(RPGme.getInstance().getUserDataFolder(), world.getUID() + ".dat");
	}
	
	public static String toString(Block block) {
		return toString(block.getChunk().getX(), block.getChunk().getZ(), block.getX(), block.getZ(), block.getY());
	}
	public static String toString(int chunkX, int chunkZ, int x, int y, int z) {
		return "" + chunkX + chunkZ + x + y + z;
	}
}
