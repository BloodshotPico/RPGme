package com.rpgme.plugin.blockmeta;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Listener;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;

/**
 * A listener class that listens for the various block change events and uses them to call a single onBreak method
 */
public abstract class BlockDataListener extends Listener<RPGme> {
	
	protected final String KEY;
	
	public BlockDataListener(RPGme plugin, String key) {
		super(plugin);
		this.KEY = key;
	}
	
	protected abstract void onBreak(Block block, Player player);
	
	protected abstract void onPiston(List<Block> block, BlockFace direction);
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerBreak(BlockBreakEvent event) {
		onBreak(event.getBlock(), event.getPlayer());
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onFlowBreak(BlockFromToEvent event) {
		onBreak(event.getToBlock(), null);
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onExplode(BlockExplodeEvent event) {
		for(Block block : event.blockList()) {
			onBreak(block, null);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onExplode(EntityExplodeEvent event) {
		for(Block block : event.blockList()) {
			onBreak(block, null);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPistonBreak(BlockPistonExtendEvent event) {
		onPiston(event.getBlocks(), event.getDirection());	
	}
	
	
	

}
