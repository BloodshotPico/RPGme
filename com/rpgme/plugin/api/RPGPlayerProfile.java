package com.rpgme.plugin.api;

import com.rpgme.plugin.player.SkillSet;

import java.util.Map;
import java.util.UUID;

/**
 *
 */
public interface RPGPlayerProfile {

    UUID getUniqueId();

    SkillSet getSkillSet();

    Map<String, String> getSettings();


}
