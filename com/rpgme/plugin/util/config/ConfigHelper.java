package com.rpgme.plugin.util.config;

import org.bukkit.plugin.Plugin;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * A utility class to help port v1 code to v2
 * Created by Robin on 02/07/2016.
 */
public class ConfigHelper {

    private static Properties messages = new Properties();

    public static void loadMessages(InputStream stream) {
        try {
            messages.load(stream);
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            try { stream.close(); } catch (IOException ignore) { }
        }
    }

    public static void prepare(Plugin plugin) {
        loadMessages(plugin.getResource("messages.properties"));
        //loadConfiguration(plugin.getResource("config.yml"));
    }

    public static void injectMessage(BundleBuilder builder, String... keys) {
        for(String key : keys) {
            injectMessage(builder, key, key);
        }
    }

    public static void injectMessage(BundleBuilder builder, String fromKey, String asKey) {
        String value = messages.getProperty(fromKey);
        if(value == null || value.isEmpty()) {
            System.err.println("Error injecting message '" + fromKey + "' as '"+asKey + "'. No such value in legacy properties");
        } else {
            builder.addValue(asKey, value);
        }
    }

    public static void injectNotification(BundleBuilder builder, Class<?> clazz, Object id) {
        String oldKey = "notification_"+clazz.getSimpleName().toLowerCase() + id;
        String key = "notification"+id;
        injectMessage(builder, oldKey, key);
    }

    public static String getNotification(BundleSection bundle, Class<?> clazz, int id, Object... formatArgs) {
        return getNotification(bundle, clazz, String.valueOf(id), formatArgs);
    }

    public static String getNotification(BundleSection bundle, Class<?> clazz, String id, Object... formatArgs) {
        String key = "notification"+id;
        return formatArgs.length > 0 ? bundle.getMessage(key, formatArgs) : bundle.getMessage(key);
    }

}
