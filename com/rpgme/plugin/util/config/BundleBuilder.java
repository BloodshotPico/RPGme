package com.rpgme.plugin.util.config;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Flamedek
 */
public class BundleBuilder {

    public static final String PREFIX_ERROR = "err";
    public static final String PREFIX_UI = "ui";
    public static final String PREFIX_ABILITY = "ability";
    public static final String PREFIX_NOTIFICATION = "notification";
    public static final String PREFIX_SKILL_DESCRIPTION = "description";

    private final List<String> commentList;
    private final List<String> keyList;
    private final List<String> valueList;

    private String prefix = "";

    private int size = 0;
    protected int maxCommentLineLength;

    public BundleBuilder() {
        this(64, 100);
    }

    public BundleBuilder(int capacity) {
        this(capacity, 100);
    }

    public BundleBuilder(int capacity, int maxCommentLineLength) {
        this.maxCommentLineLength = maxCommentLineLength;
        commentList = new ArrayList<>(capacity);
        keyList = new ArrayList<>(capacity);
        valueList = new ArrayList<>(capacity);
    }

    public int size() {
        return size;
    }

    public String getPrefix() {
        return prefix;
    }

    public BundleBuilder setPrefix(String prefix) {
        this.prefix = prefix == null || prefix.isEmpty() ? "" : (prefix + "_");
        return this;
    }

    /**
     * The getting part
     */

    public String get(String key) {
        int index = keyList.indexOf(key);
        return index >= 0 ? valueList.get(index) : MessagesBundle.MESSAGE_UNKNOWN;
    }

    public List<String> keyList() {
        List<String> keyOnlyList = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            String key = keyList.get(i);
            String value = valueList.get(i);
            if(!key.isEmpty() && !value.isEmpty()) {
                keyOnlyList.add(key);
            }
        }
        return keyOnlyList;
    }

    public List<String> valueList() {
        List<String> valueOnlyList = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            String key = keyList.get(i);
            String value = valueList.get(i);
            if(!key.isEmpty() && !value.isEmpty()) {
                valueOnlyList.add(value);
            }
        }
        return valueOnlyList;
    }

    public LinkedHashMap<String, String> getValues() {
        List<String> keys = keyList();
        List<String> values = valueList();
        int size = keys.size();

        LinkedHashMap<String, String> map = new LinkedHashMap<>(size);
        for(int i = 0; i < size; i++) {
            String key = keys.get(i);
            String value = values.get(i);

            map.put(key, value);
        }
        return map;
    }

    public MessagesBundle build(){
        return new MessagesBundle(getValues());
    }

    /**
     * The content adding part
     */

    /**
     * Add an entry to this properties bundle. This can represent a comment section, whitespace or configurable entry.
     * All arguments may be the empty string or null.
     * @param comment if not empty, will print every line prefixed with " # "
     * @param key if not empty, will print the key folowed by the value
     * @param value the default value for this key
     */
    public BundleBuilder addValue(String comment, String key, String value) {
        commentList.add(fromNull(comment));
        keyList.add(fromNull(key).isEmpty() ? "" : (prefix + fromNull(key)));
        valueList.add(fromNull(value));
        size++;
        return this;
    }

    /**
     * Add an configurable setting without a comment
     * @param key
     * @param value
     */
    public BundleBuilder addValue(String key, String value) {
        return addValue(null, key, value);
    }

    /**
     * Add only a comment in the current position.
     * The string does not have to be commented with # and is split to length for long messages.
     * @param comment
     */
    public BundleBuilder addComment(String comment) {
        return addValue(comment, null, null);
    }

    public BundleBuilder addLargeHeader(String text) {
        StringBuilder builder = new StringBuilder(256);
        final String headerPart = StringUtils.repeat('=', 40);

        // top
        builder.append(headerPart).append("  ").append(headerPart).append(" #\n");
        // title
        int availableLength = headerPart.length() * 2 + 2 - 6;
        int titleLength = text.length();
        int preLength = (availableLength - Math.min(titleLength, availableLength)) / 2;
        int postLength = availableLength - titleLength - preLength;
        builder.append("== ").append(StringUtils.repeat(' ', preLength)).append(text).append(StringUtils.repeat(' ', postLength)).append(" == #\n");
        // bottom
        builder.append(headerPart).append("  ").append(headerPart).append(" #\n");

        return addComment(builder.toString());
    }

    public BundleBuilder addHeader(String text) {
        int availableLength = 25 * 2 + 2 - 6;
        int titleLength = text.length();
        int preLength = Math.max(1, (availableLength - Math.min(titleLength, availableLength)) / 2);
        int postLength = Math.max(1, availableLength - titleLength - preLength);

        StringBuilder header = new StringBuilder().append(StringUtils.repeat('=', preLength)).append("  ").append(text).append("  ").append(StringUtils.repeat('=', postLength)).append(" #");
        return addComment(header.toString());
    }

    public BundleBuilder addNewline() {
        return addValue("\n", null, null);
    }

    /**
     * The content reading part
     */

    public void load(File file) {
        try {
            load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void load(InputStream stream) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream))){

            String line;
            while((line = reader.readLine()) != null) {
                line = line.trim();
                if(line.startsWith("#")) {
                    line = line.substring(1).trim();
                    addComment(line);
                    continue;
                }

                int seperator = line.indexOf('=');
                if(seperator == -1) {
                    if(line.isEmpty()) {
                        addNewline();
                    } else {
                        System.err.println("Error reading stream. unknown line: '" + line + '\'');
                    }
                    continue;
                }
                String key = line.substring(0, seperator);
                String value = line.substring(seperator+1).trim();

                addValue(null, key, value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The editing part
     */

    public boolean set(String key, String value) {
        int index = keyList.indexOf(key);
        if(index >= 0) {
            valueList.set(index, value);
            return true;
        }
        return false;
    }

    public void setAll(Map<String, String> values) {
        for(Map.Entry<String, String> entry : values.entrySet()) {
            set(entry.getKey(), entry.getValue());
        }
    }

    public void setAll(BundleBuilder other) {
        for(int i = 0; i < other.size; i++) {
            set(other.keyList().get(i), other.valueList().get(i));
        }
    }

    /**
     * The writing part
     */

    private Writer writer;

    public void writeToFile(File file) {
        if(!file.exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            write(new BufferedWriter(new FileWriter(file)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean write(Writer writer) {
        this.writer = writer;
        try {
            for (int i = 0; i < size; i++) {
                writeIndex(i);
            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                writer.close();
            } catch (IOException e) { }
        }
    }

    private void writeIndex(int i) throws IOException {
        String comment = commentList.get(i);
        String key = keyList.get(i);
        String value = valueList.get(i);

        // write values
        if(!comment.isEmpty()) {
            if(comment.equals("\n"))
                writeNewline();
            else
                writeComment(comment);
        }
        if(!key.isEmpty()) {
            writeKey(key);
            if(!value.isEmpty()) {
                writeValue(value);
            }
            writer.write('\n');
        }
    }

    private void writeNewline() throws IOException {
        writer.write('\n');
        writer.flush();
    }

    private void writeKey(String key) throws IOException {
        writer.write(key);
        writer.write("=");
    }

    private void writeValue(String value) throws IOException {
        writer.write(value.replace("\n", String.valueOf('\\') + String.valueOf('n')));
    }

    private void writeComment(String comment) throws IOException {
        for(String line : splitToLength(comment)) {
            writer.write("# ");
            writer.write(line);
            writer.write("\n");
        }

    }

    private List<String> splitToLength(String input) {
        List<String> list = new ArrayList<>(5);
        StringBuilder buff = new StringBuilder();
        String split = " ";

        for(String line : input.split("\\n")) {

            for(String s : line.split(split)) {

                if(buff.length() + s.length() + 1 > maxCommentLineLength) {
                    list.add(buff.toString());
                    buff.delete(0, buff.length());
                }
                buff.append(s).append(' ');
            }
            if(buff.length() > 0) {
                list.add(buff.toString());
                buff.delete(0, buff.length());
            }
        }

        return list;
    }

    public void combineWith(File file) {
        // if the file does not exist, write the current state in the file
        if(!file.exists()) {
            writeToFile(file);
            System.out.println("Placed default "+file.getName());
            return;
        }

        // load the file config
        BundleBuilder other = new BundleBuilder(64);
        other.load(file);

        List<String> otherKeys = other.keyList();
        List<String> otherValues = other.valueList();

        // write all values from the file config into this config
        for(int i = 0; i < otherKeys.size(); i++) {
            String key = otherKeys.get(i);
            String value = otherValues.get(i);

            boolean overridden = set(key, value);
            if(!overridden) {
                // no access to a Logger right now
                //System.err.println("BundleBuilder: Ignored key " + key + " in file because it is not in the current configuration.");
            }
        }

        // check if the file is up to date
        boolean upToDate = otherKeys.containsAll(keyList());
        if(!upToDate) {
            // if not, write our current state over the file. It now contains all settings from the original.
            writeToFile(file);
            System.out.println("File "+file.getName() + " has been updated with new settings!");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        List<String> key = keyList();
        List<String> value = valueList();
        for(int i = 0; i < key.size(); i++) {
            builder.append(key.get(i)).append(":").append(value.get(i)).append(", ");
        }
        return builder.append(']').toString();
    }



    private static String toNull(String string) {
        return string == null ? null : string.isEmpty() ? null : string;
    }

    private static String fromNull(String string) {
        return string == null ? "" : string;
    }

}

