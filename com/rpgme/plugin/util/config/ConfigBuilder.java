package com.rpgme.plugin.util.config;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  Configuration utility class to dynamically build a configuration.
 *  It features methods to:
 *      - define the (default) entries in code, including comments
 *      - this creates the config to write to file
 *      - or read from inputstream, excluding comments
 *      - set key-value pairs
 *      - synchronize the whole generated configuration with the version in a file
 * @author Flamedek
 */
public class ConfigBuilder {

    private static final int INDENT = 2;
    private static final String SECTION_END = ".";

    private final List<String> commentList;
    private final List<String> keyList;
    private final List<Object> valueList;

    private final List<String> currentPath = new ArrayList<>(8);

    private int size = 0;
    protected int maxCommentLineLength;

    private boolean hasValueInCurrentSection = false;

    public ConfigBuilder() {
        this(64, 100);
    }

    public ConfigBuilder(int capacity) {
        this(capacity, 100);
    }

    public ConfigBuilder(int capacity, int maxCommentLineLength) {
        this.maxCommentLineLength = maxCommentLineLength;
        commentList = new ArrayList<>(capacity);
        keyList = new ArrayList<>(capacity);
        valueList = new ArrayList<>(capacity);
    }

    public int size() {
        return keyList().size();
    }

    /**
     * The getting part
     */

    public Object get(String key) {
        int index = keyList.indexOf(key);
        return index >= 0 ? valueList.get(index) : 0;
    }

    public List<String> keyList() {
        List<String> keyOnlyList = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            String key = keyList.get(i);
            Object value = valueList.get(i);
            if(key != null && !key.equals(SECTION_END) && value != null) {
                keyOnlyList.add(key);
            }
        }
        return keyOnlyList;
    }

    public List<Object> valueList() {
        List<Object> valueOnlyList = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            String key = keyList.get(i);
            Object value = valueList.get(i);
            if(key != null &&!key.equals(SECTION_END) && value != null) {
                valueOnlyList.add(value);
            }
        }
        return valueOnlyList;
    }

    public Map<String, Object> getValues() {
        List<String> keys = keyList();
        List<Object> values = valueList();

        int size = keys.size();
        Map<String, Object> map = new HashMap<>(size);
        for(int i = 0; i < size; i++) {
            String key = keys.get(i);
            Object value = values.get(i);

            map.put(key, value);
        }
        return map;
    }

    public FileConfiguration build() {
        YamlConfiguration config = new YamlConfiguration();
        config.addDefaults(getValues());
        return config;
    }

    public String getCurrentPath() {
        return String.join(".", currentPath);
    }


    /**
     * The content adding part
     */

    public ConfigBuilder addValue(String comment, String key, Object value) {
        commentList.add(toNull(comment));
        if((key = toNull(key)) != null) {
            if(key.indexOf('.') == -1) {
                key = currentPath.isEmpty() ? key : String.join(".", currentPath) + "." +  key;
            }

        }
        keyList.add(key);
        valueList.add(value);
        size++;

        hasValueInCurrentSection = true;
        return this;
    }

    public ConfigBuilder addValue(String key, Object value) {
        return addValue(null, key, value);
    }

    public ConfigBuilder addLargeHeader(String text) {
        StringBuilder builder = new StringBuilder(256);

        final String headerPart = StringUtils.repeat('=', 40);

        // top
        builder.append(headerPart).append("  ").append(headerPart).append(" #\n");
        // title
        int availableLength = headerPart.length() * 2 + 2 - 6;
        int titleLength = text.length();
        int preLength = (availableLength - titleLength) / 2;
        int postLength = availableLength - titleLength - preLength;
        builder.append("== ").append(StringUtils.repeat(' ', preLength)).append(text).append(StringUtils.repeat(' ', postLength)).append(" == #\n");
        // bottom
        builder.append(headerPart).append("  ").append(headerPart).append(" #\n");

        return addComment(builder.toString());
    }

    public ConfigBuilder addHeader(String text) {
        int availableLength = 30 * 2 + 2 - 6;
        int titleLength = text.length();
        int preLength = Math.max(1, (availableLength - Math.min(titleLength, availableLength)) / 2);
        int postLength = Math.max(1, availableLength - titleLength - preLength);

        String header = new StringBuilder().append(StringUtils.repeat('=', preLength))
                .append("  ").append(text).append("  ")
                .append(StringUtils.repeat('=', postLength))
                .append(" #").toString();
        return addComment(header);
    }

    public ConfigBuilder addComment(String comment) {
        return addValue(comment, null, null);
    }

    public ConfigBuilder beginSection(String name) {
        return beginSection(null, name);
    }

    public ConfigBuilder beginSection(String comment, String name) {
        addValue(comment, name, null);
        currentPath.add(name);
        hasValueInCurrentSection = false;
        return this;
    }

    public ConfigBuilder endSection() {
        currentPath.remove(currentPath.size()-1);
        if(hasValueInCurrentSection) {
            addValue("\n", SECTION_END, "");
        } else {
            removeLastValue();
        }
        return this;
    }

    public ConfigBuilder addNewline() {
        return addValue("\n", null, null);
    }

    private void removeLastValue() {
        int index = commentList.size() -1;
        commentList.remove(index);
        keyList.remove(index);
        valueList.remove(index);
        size--;
    }

    /**
     * The content reading part
     */

    public void load(File file) {
        try {
            load(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public void load(InputStream stream) {
        Yaml yaml = new Yaml();
        Map<String, Object> map = (Map<String, Object>) yaml.load(stream);
        map.entrySet().forEach((entry) -> loadEntry("", entry));
    }

    @SuppressWarnings("unchecked")
    private void loadEntry(String path, Map.Entry<String, Object> entry) {
        if(path == null) path = "";
        Object obj = entry.getValue();
        if(obj instanceof Map) {

            Map<String, Object> map = (Map<String, Object>) obj;
            String innerPath = path.isEmpty() ?
                    entry.getKey() + "." :
                    path + entry.getKey() + ".";

            //path + (path.isEmpty() ? "" : ".") + entry.toString();

            map.entrySet().forEach((innerEntry) -> loadEntry(innerPath, innerEntry));
        } else {
            addValue(path + entry.getKey(), entry.getValue());
        }
    }

    /**
     * The editing part
     */

    public boolean set(String key, Object value) {
        int index = keyList.indexOf(key);
        if(index >= 0) {
            valueList.set(index, value);
            return true;
        }
        return false;
    }

    public void setAll(Map<String, Object> values) {
        for(Map.Entry<String, Object> entry : values.entrySet()) {
            set(entry.getKey(), entry.getValue());
        }
    }

    public void setAll(ConfigBuilder other) {
        for(int i = 0; i < other.size; i++) {
            set(other.keyList().get(i), other.valueList().get(i));
        }
    }

    /**
     * The writing part
     */

    private Writer writer;
    private Yaml yaml;

    private int indent = 0;

    public void writeToFile(File file) {
        if(!file.exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            write(new BufferedWriter(new FileWriter(file)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean write(Writer writer) {
        this.writer = writer;
        yaml = new Yaml();
        try {
            for (int i = 0; i < size; i++) {
                writeIndex(i);
            }
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                writer.close();
            } catch (IOException e) { }
        }
    }

    private void writeIndex(int i) throws IOException {
        String comment = commentList.get(i);
        String key = keyList.get(i);
        Object value = valueList.get(i);

        // update indent
        if (SECTION_END.equals(key)) {
            indent -= INDENT;
        } else if (key != null) {
            indent = StringUtils.countMatches(key, ".") * INDENT;
        }
        // write comment
        if(comment != null) {
            if(comment.equals("\n"))
                writeNewline();
            else
                writeComment(comment);
        }
        // write key/value
        if(key != null && !key.equals(SECTION_END)) {
            writeKey(key);
            if(value != null) {
                writeValue(value);
            } else {
                writer.write('\n');
            }
        }
    }

    private void writeNewline() throws IOException {
        writer.write('\n');
        writer.flush();
    }

    private void writeKey(String key) throws IOException {
        int index = StringUtils.lastIndexOf(key, '.');
        if(index >= 0) {
            key = key.substring(index+1);
        }
        writeIndent();
        writer.write(key);
        writer.write(": ");
    }

    private void writeValue(Object value) throws IOException {
        String string = yaml.dump(value);
        writer.write(string);
    }

    private void writeIndent() throws IOException {
        for(int i = 0; i < indent; i++) {
            writer.append(' ');
        }
    }

    private void writeComment(String comment) throws IOException {
        for(String line : splitToLength(comment)) {
            writeIndent();
            writer.write("# ");
            writer.write(line);
            writer.write("\n");
        }

    }

    private List<String> splitToLength(String input) {
        List<String> list = new ArrayList<>(5);
        StringBuilder buff = new StringBuilder();
        String split = " ";

        for(String line : input.split("\n")) {

            for(String s : line.split(split)) {

                if(buff.length() + s.length() + 1 > maxCommentLineLength) {
                    list.add(buff.toString());
                    buff.delete(0, buff.length());
                }
                buff.append(s).append(' ');
            }
            if(buff.length() > 0) {
                list.add(buff.toString());
                buff.delete(0, buff.length());
            }
        }

        return list;
    }

    /**
     * A powerful method that synchronizes the content of this builder with a remote file.
     * This should be used after the building of this builder is complete.
     * The file is read, and the configurations are compared following the following guides:
     * <ul>
     * <li>values that are in this builder, but are not in the file will be written to the file</li>
     * <li>values that are both in this builder and the file, are read and put into this builder.</li>
     * <li>values that are not in this builder, but are in the file are logged and ignored.</li>
     * </ul>
     * @param file the file to synchronize with. If it does not exist, it will be created.
     */
    public void combineWith(File file) {
        // if the file does not exist, write the current state in the file
        if(!file.exists()) {
            writeToFile(file);
            System.out.println("Placed default "+file.getName());
            return;
        }

        // load the current config in the file
        ConfigBuilder other = new ConfigBuilder(64);
        other.load(file);

        List<String> otherKeys = other.keyList();
        List<Object> otherValues = other.valueList();

        // write all values of the other config into this config
        for(int i = 0; i < other.size(); i++) {
            String key = otherKeys.get(i);
            Object value = otherValues.get(i);

            boolean overridden = set(key, value);
            if(!overridden) {
                //System.err.println("[ConfigBuilder]: Ignored key " + key + " in file because it is not in the current configuration.");
            }
        }

        // check if the file is up to date
        boolean isFileUpToDate = otherKeys.containsAll(keyList());
        if(!isFileUpToDate) {
            // if not, write our current state over the file. It now contains all settings from the original.
            writeToFile(file);
            System.out.println("File "+file.getName() + " has been updated with new settings!");
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("ConfigBuilder[");
        List<String> key = keyList();
        List<Object> value = valueList();
        for(int i = 0; i < key.size(); i++) {
            builder.append(key.get(i)).append(":").append(value.get(i)).append(", ");
        }
        return builder.append(']').toString();
    }

    private static String toNull(String string) {
        return string == null || string.isEmpty() ? null : string;
    }

    private static String fromNull(String string) {
        return string == null ? "" : string;
    }

}
