package com.rpgme.plugin.util;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.integration.PluginIntegration;
import com.rpgme.plugin.player.RPGPlayer;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.List;

public class PermissionChecker {

	// large all-inclusive checks
	public static boolean isEnabled(Player player) {
		return player != null && !isNPC(player) && !isInDisabledWorld(player) && !isInDisabledRegion(player) && isLoaded(player);
	}

	public static boolean isEnabled(Player player, int skill) {
		return isEnabled(player, RPGme.getInstance().getSkill(skill));
	}

	public static boolean isEnabled(Player player, Skill skill) {
		return skill != null && isEnabled(player) && hasPermission(player, skill);
	}

	public static boolean isEnabled(RPGPlayer player, int skill) {
		return isEnabled(player, RPGme.getInstance().getSkill(skill));
	}

	public static boolean isEnabled(RPGPlayer player, Skill skill) {
		return skill != null && player != null && !isInDisabledWorld(player.getPlayer()) && !isInDisabledRegion(player.getPlayer()) && hasPermission(player.getPlayer(), skill);
	}

	// config
	private static boolean doPermissionCheck = RPGme.getInstance().getConfig().getBoolean("Per skill permissions", false);

	// seperate single checks
	public static boolean hasPermission(Player player, Skill skill) {
		if(!doPermissionCheck)
			return true;
		if(skill == null)
			return false;

		String permission = "rpgme.skill."+skill.getName().toLowerCase();
		return player.hasPermission(permission);
	}

	public static boolean isInDisabledWorld(Player player) {
		return isDisabledWorld(player.getWorld());
	}

	public static boolean isDisabledWorld(World world) {
		List<String> list = RPGme.getInstance().getConfig().getStringList("Disabled in worlds");
		return list != null && list.contains(world.getName());
	}

	public static boolean isInDisabledRegion(Player player) {
		return !PluginIntegration.getInstance().isRPGmeEnabled(player, player.getLocation());
	}

	public static boolean isLoaded(Player p) {
		return RPGme.getInstance().getPlayerManager().isLoaded(p);
	}
	/** checks if an entity is an Citizens npc */
	public static boolean isNPC(Entity e) {
		return e.hasMetadata("NPC");
	}



}
