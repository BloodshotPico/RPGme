package com.rpgme.plugin.util.cooldown;

import com.google.common.collect.Maps;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class VarEnergyPerSecCooldown implements Cooldown {

    private final Map<UUID, Entry> map = Maps.newHashMap();
    private final int maxenergy, energycost;

    public VarEnergyPerSecCooldown(Plugin plugin, int energycost, int maxenergy) {
        CooldownCleaner.register(this, plugin);
        this.maxenergy = maxenergy;
        this.energycost = energycost;
    }

    @Override
    public synchronized boolean isOnCooldown(Player p) {
        Entry e = map.get(p.getUniqueId());
        return e != null && e.getCurrentEnergy() < energycost;
    }

    @Override
    public void add(Player p) {
        throw new UnsupportedOperationException("Use register(Player, int) instead.");
    }

    @Override
    public synchronized void add(Player p, long energypersec) {
        Entry e = map.get(p.getUniqueId());
        if(e == null) {
            e = new Entry(energypersec/1000.0);
            map.put(p.getUniqueId(), e);
        } else {
            e.energyPerSec = energypersec / 1000.0;
        }
        e.onUse();
    }

    @Override
    public synchronized boolean remove(Player p) {
        return map.remove(p.getUniqueId()) != null;
    }

    @Override
    public synchronized long getMillisRemaining(Player p) {
        Entry e = map.get(p.getUniqueId());
        if(e == null)
            return 0l;
        double current = e.getCurrentEnergy();
        if(current >= energycost)
            return 0l;

        return (long) ((energycost - current) / e.energyPerSec * 1000l);
    }

    @Override
    public synchronized void cleanUp() {
        Iterator<Map.Entry<UUID, Entry>> it = map.entrySet().iterator();
        while(it.hasNext()) {
            Entry e = it.next().getValue();
            if(e.energy == maxenergy)
                it.remove();

        }
    }

    public class Entry {

        private long lastUsed;
        private double energy;
        private double energyPerSec;

        public Entry(double energyPerSec) {
            this.lastUsed = System.currentTimeMillis();
            this.energyPerSec = energyPerSec;
            this.energy = maxenergy;
        }

        double getCurrentEnergy() {
            return Math.min(maxenergy, (energy + ((System.currentTimeMillis() - lastUsed) / 1000.0 * energyPerSec)) );
        }

        void onUse() {
            energy = getCurrentEnergy() - energycost;
            lastUsed = System.currentTimeMillis();
        }

    }

}
