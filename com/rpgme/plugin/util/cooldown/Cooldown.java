package com.rpgme.plugin.util.cooldown;

import org.bukkit.entity.Player;

public interface Cooldown extends Cleanable {
	
	boolean isOnCooldown(Player p);
	
	void add(Player p);
	
	// optional
	void add(Player p, long extra);
	
	boolean remove(Player p);
		
	long getMillisRemaining(Player p);
	

}
