package com.rpgme.plugin.util.cooldown;

import com.google.common.collect.Sets;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Set;

public class CooldownCleaner {
	
	private static Set<Cleanable> cooldowns = Sets.newHashSet();
	private static CooldownTask task = new CooldownTask();
	
	public static void register(Cleanable cooldown, Plugin plugin) {
		cooldowns.add(cooldown);
		task.start(plugin);
	}
	
	private static class CooldownTask extends BukkitRunnable {
		
		boolean running = false;
		
		public void start(Plugin plugin) {
			if(!running) {
				running = true;
				runTaskTimerAsynchronously(plugin, 1200L, 200L);
			}
		}
		
		@Override
        public void run() {
			for(Cleanable c : cooldowns) {
                c.cleanUp();
            }
		}
		
		
	}

}
