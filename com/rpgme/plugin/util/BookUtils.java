package com.rpgme.plugin.util;

import com.rpgme.plugin.util.nbtlib.NBTFactory;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.chat.ComponentSerializer;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by Robin on 07/08/2016.
 */
@SuppressWarnings("unchecked")
public class BookUtils {

    private static Method chatSerializerMethod;
    private static Field pagesField;

    private static Constructor<?> packetDataSerializerConstructor;
    private static Constructor<?> packetPlayOutCustomPayloadConstructor;

    private static Method getHandleMethod;
    private static Field playerConnectionField;
    private static Method sendPacketMethod;

    static {
        try {
            // last tested version: 1.9.4

            String nmsPath = NBTFactory.NMS_PATH + ".";
            String obcPath = NBTFactory.CRAFT_PATH + ".";

            Class<?> chatSerializeClass = Class.forName(nmsPath + "IChatBaseComponent$ChatSerializer");
            chatSerializerMethod = chatSerializeClass.getMethod("a", String.class);

            Class<?> craftMetaBookClass = Class.forName(obcPath + "inventory.CraftMetaBook");
            pagesField = craftMetaBookClass.getField("pages");


            Class<?> packetDataSerializerClass = Class.forName(nmsPath + "PacketDataSerializer");
            packetDataSerializerConstructor = packetDataSerializerClass.getConstructor(ByteBuf.class);

            Class<?> packetPayloadClass = Class.forName(nmsPath + "PacketPlayOutCustomPayload");
            packetPlayOutCustomPayloadConstructor = packetPayloadClass.getConstructor(String.class, packetDataSerializerClass);


            Class<?> craftPlayerClass = Class.forName(obcPath + "entity.CraftPlayer");
            getHandleMethod = craftPlayerClass.getMethod("getHandle");

            Class<?> nmsPlayerClass = Class.forName(nmsPath + "EntityPlayer");
            playerConnectionField = nmsPlayerClass.getField("playerConnection");

            Class<?> playerConnectionClass = Class.forName(nmsPath + "PlayerConnection");
            Class<?> packetClass = Class.forName(nmsPath + "Packet");
            sendPacketMethod = playerConnectionClass.getMethod("sendPacket", packetClass);

        } catch (ClassNotFoundException | NoSuchFieldException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens a book gui for a player with the given ComponentBuilders as pages.
     * ComponentBuilder features ChatColors and hover/click events.
     * The player does not actually have to be holding or using a book item, this method just opens the GUI for them.
     */
    public static void openBook(Player player, ComponentBuilder... pages) {
        try {
            // prepare the meta
            BookMeta meta = (BookMeta) player.getServer().getItemFactory().getItemMeta(Material.WRITTEN_BOOK);
            List pageList = (List) pagesField.get(meta);

            for (ComponentBuilder page : pages) {
                // prepare the text
                String jsonText = ComponentSerializer.toString(page.create());
                Object iChatBaseComponent = chatSerializerMethod.invoke(null, jsonText);
                // add as page to the meta
                pageList.add(iChatBaseComponent);
            }

            ItemStack bookItem = new ItemStack(Material.WRITTEN_BOOK);
            bookItem.setItemMeta(meta);

            // set the book
            int slot = player.getInventory().getHeldItemSlot();
            final ItemStack currentItem = player.getInventory().getItem(slot);
            player.getInventory().setItem(slot, bookItem);

            // send open packet
            ByteBuf buffer = Unpooled.buffer(256);
            buffer.setByte(0, 0);
            buffer.writerIndex(1);

            Object dataSerializer = packetDataSerializerConstructor.newInstance(buffer);
            Object packet = packetPlayOutCustomPayloadConstructor.newInstance("MC|BOpen", dataSerializer);

            Object nmsPlayer = getHandleMethod.invoke(player);
            Object playerConnection = playerConnectionField.get(nmsPlayer);
            sendPacketMethod.invoke(playerConnection, packet);

            // reset previous item
            player.getInventory().setItem(slot, currentItem);

        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the given ComponentBuilders as meta for a book ItemStack.
     * ComponentBuilder features ChatColors and hover/click events.
     */
    public void setBookPages(ItemStack item, ComponentBuilder... pages) {
        try {

            BookMeta meta = (BookMeta) item.getItemMeta();
            List pageList = (List) pagesField.get(meta);
            pageList.clear();

            for (ComponentBuilder page : pages) {
                // prepare the text
                String jsonText = ComponentSerializer.toString(page.create());
                Object iChatBaseComponent = chatSerializerMethod.invoke(null, jsonText);
                // add as page to the meta
                pageList.add(iChatBaseComponent);
            }

        } catch (ClassCastException e) {
            throw new IllegalArgumentException("ItemStack does not have BookMeta (incompatible type)");
        }
        catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

}
