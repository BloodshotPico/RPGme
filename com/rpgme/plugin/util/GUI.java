package com.rpgme.plugin.util;


import com.rpgme.plugin.api.Listener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class GUI<P extends JavaPlugin> extends Listener<P> {
	
	public final Player player;
	private final Inventory inventory;
    private final String name;
 
    /**
    * Creates a new GUI display.
    *
    * @param player Player to show the GUI.
    * @param guiName The GUI display name.
    * @param rows The GUI slots. Must be multiple of 9!
    */
	public GUI(P plugin, Player player, String guiName, int rows)
    {
    	super(plugin);
        this.name = guiName;
        this.player = player;
        
        inventory = Bukkit.getServer().createInventory(null, rows*9, ChatColor.translateAlternateColorCodes('&', guiName));
    }
 
    /**
    * Adds items to the GUI.
    */
    public abstract void addItems();
 
    /**
    * Sets the action to do when clicked.
    */
    public abstract void doAction(int slot);

    /**
     * Creates a GUI item.
     * @param guiSlot the slot of inventory to occupy
     * @param item the item
     */
    public void addItem(int guiSlot, ItemStack item)
    {
        inventory.setItem(guiSlot, item);
    }
 
    @EventHandler
    public void onInventoryItemClick(final InventoryClickEvent e)
    {
        // Checking if the inventory is the correct one.
 
        if (!e.getInventory().equals(inventory))
            return;
        
        if (e.getCurrentItem() == null || !e.getCurrentItem().hasItemMeta() || !e.getCurrentItem().getItemMeta().hasDisplayName() )
        {
            return;
        }
         
        e.setCancelled(true);
 
        // Waiting for the event to getModule cancelled.
 
        doAction(e.getSlot());
    }
    
    @EventHandler
    public void onClose(InventoryCloseEvent event) {
    	if(event.getInventory().equals(inventory))
    		unregister();
    }
 
    /**
    * Gets the GUI name.
    *
    * @return
    */
    public String getGuiName()
    {
        return name;
    }
 
    /**
    * Opens the GUI.
    */
    public void openGUI()
    {
    	registerListeners();
    	player.openInventory(inventory);
    }
 
    /**
    * Closes the GUI.
    */
    public void closeGUI()
    {
        player.closeInventory();
    }
 
    /**
    * Gets the GUI.
    *
    * @return
    */
    public Inventory getGUI()
    {
        return inventory;
    }
 
    public void unregister() {
    	InventoryClickEvent.getHandlerList().unregister(this);
    	InventoryCloseEvent.getHandlerList().unregister(this);
    }

}
