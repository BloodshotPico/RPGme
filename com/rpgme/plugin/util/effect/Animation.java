package com.rpgme.plugin.util.effect;

import org.apache.commons.lang.Validate;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public abstract class Animation <A> extends BukkitRunnable {

	private final A[] content;

	private final boolean infinite, sync;
	private final int speed;

	private int step = 0;

	public Animation(A[] content, int speed, boolean infinite, boolean sync) {
		Validate.notNull(content);

		this.content = content;
		this.infinite = infinite;
		this.speed = speed;
		this.sync = sync;
	}


	public BukkitTask runAnimation(Plugin plugin) {
		if(sync)
			return runTaskTimer(plugin, 1l, speed);
		else
			return runTaskTimerAsynchronously(plugin, 1l, speed);
	}

	@Override
	public void run() {

		if(step == content.length && !infinite) {
			cancel();
		} else {

			A next = (content[step++ % content.length]);

			doStep(next);

		}

	}

	public boolean isInfinite() {
		return infinite;
	}

	public abstract void doStep(A value);



}
