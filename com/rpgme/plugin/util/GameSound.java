package com.rpgme.plugin.util;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class GameSound {
	
//	HEAL(Sound.NOTE_PIANO, 1.0f, 2f),
//	SPEED_BOOST(Sound.ENDERDRAGON_HIT, 1.3f, 0.8f),
//	RAGE(Sound.WOLF_HOWL, 1.0f, 0.75f),
//	VOLLEY_LOADED (Sound.NOTE_PIANO, 1.8f, 1.3f),
//	POTION_CLOUD (Sound.FIZZ, 0.4f, 1f),
//	BLOODLUST (Sound.ENDERDRAGON_WINGS, 1f, 1f),
//	HEADSHOT (Sound.SUCCESSFUL_HIT, 1f, 1.1f),
//	
//	LEVEL_UP(Sound.LEVEL_UP, 1.0f, 1.2f),
//	EXP_TOMB(Sound.WITHER_SHOOT, 1f, 1.2f),
//	SKILL_NOTIFICATION(Sound.LEVEL_UP, 1.6f, 1.1f),
//	RESPAWN(Sound.WITHER_SPAWN, 0.65f, 1.1f),
//	
//	NOT_ALLOWED (Sound.ENTITY_VILLAGER_NO, 1.5f, 0.85f), 
//	COUNTDOWN_END (Sound.NOTE_PIANO, 1.5f, 0.8f),
//	MENU (Sound.SUCCESSFUL_HIT, 1f, 0.9f),
//	
//	TREASURE(Sound.NOTE_PIANO, 2f, 1.4f),
//	
//	WOOD_BREAK (Sound.FALL_BIG, 1.5f, 0.8f),
//	FORGING_CRAFTED (Sound.ZOMBIE_METAL, 0.7f, 1.35f),
//	ENCHANTMENT_ADDED (Sound.ORB_PICKUP, 0.8f, 1.2f),
//	
//	POWERTOOL_ACTIVATE(Sound.HORSE_SADDLE, 1.2f, 1.2f),
//	GROUND_SLAM(Sound.EXPLODE, 1.5f, 0.4f),
//	
//	POTION_COMPLETE (Sound.BURP, 0.5f, 1f), 
//	;
//	
//	
//	private GameSound(Sound sound, float volume, float pitch) {
//		this(sound, volume, pitch, null);
//	}
//	
//	private GameSound(Sound sound, float volume, float pitch, Double offset) {
//		this.sound = sound; this.volume = volume; this.pitch = pitch; this.offset = offset;
//	}
//	private final Sound sound;
//	private final float volume;
//	private final float pitch;
//	private final Double offset;
//	
//	public void play(Player player) {
//		if(offset != null)
//			play(player, volume, pitch, offset);
//		else
//			play(player, volume, pitch);
//	}
//	
//	public void play(Player player, float volume, float pitch) {
//		player.playSound(player.getLocation(), sound, volume, pitch);
//	}
//	
//	public void play(Player player, double offset) {
//		play(player, volume, pitch, offset);
//	}
//	public void play(Player player, float volume, float pitch, double offset) {
//		double rand = (CoreUtils.random.nextDouble() * 2 - 1 ) * offset;
//		player.playSound(player.getLocation(), sound, (float) (volume + (rand / 2)), (float) (pitch + rand));
//	}
//	
//	public void play(Location loc) {
//		if(offset != null)
//			play(loc, volume, pitch, offset);
//		else
//			play(loc, volume, pitch);
//	}
//	public void play(Location loc, double offset) {
//		play(loc, volume, pitch, offset);
//	}
//	public void play(Location loc, float volume, float pitch) {
//		loc.getWorld().playSound(loc, sound, volume, pitch);
//	}
//	public void play(Location loc, float volume, float pitch, double offset) {
//		double rand = (CoreUtils.random.nextDouble() * 2 - 1 ) * offset;
//		loc.getWorld().playSound(loc, sound, (float) (volume + (rand / 2)), (float) (pitch + rand));
//	}
//
//
	
	public static void play(Sound sound, Player player) {
		play(sound, player, 1f, 1f);
	}
	
	public static void play(Sound sound, Player player, float volume, float pitch) {
		player.playSound(player.getLocation(), sound, volume, pitch);
	}
	
	public static void play(Sound sound, Player player, double offset) {
		play(sound, player, 1f, 1f, offset);
	}
	public static void play(Sound sound, Player player, float volume, float pitch, double offset) {
		double rand = (CoreUtils.random.nextDouble() * 2 - 1 ) * offset;
		player.playSound(player.getLocation(), sound, volume, (float) (pitch + rand));
	}
	
	public static void play(Sound sound, Location loc) {
		play(sound, loc, 1f, 1f);
	}
	public static void play(Sound sound, Location loc, double offset) {
		play(sound, loc, 1f, 1f, offset);
	}
	public static void play(Sound sound, Location loc, float volume, float pitch) {
		loc.getWorld().playSound(loc, sound, volume, pitch);
	}
	public static void play(Sound sound, Location loc, float volume, float pitch, double offset) {
		double rand = (CoreUtils.random.nextDouble() * 2 - 1 ) * offset;
		loc.getWorld().playSound(loc, sound, volume, (float) (pitch + rand));
	}

}
