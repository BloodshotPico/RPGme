package com.rpgme.plugin.util.menu;

import com.google.common.collect.Lists;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.GameSound;
import com.rpgme.plugin.util.ItemUtils;
import com.rpgme.plugin.util.Symbol;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class Settings {

	public static void addToggleMenu(List<Entry<String, ItemStack>> map) {
		List<String> list = Lists.newArrayList();

		for(Entry<String, ItemStack> e : map) {

			for(Entry<String, ItemStack> others : map) {

				String key = others.getKey();
				int spaces = (int) Math.ceil((25 - key.length()) / 2.0);
				if(key.equalsIgnoreCase(e.getKey())) {
					key = "&e» &f" + key + "&e «";
					spaces -= 2;
				} else 
					key = "&7" + key;

				list.add(StringUtils.repeat(' ', spaces) + key);
			}

			e.setValue(ItemUtils.addToLore(e.getValue(), list));
			list.clear();

		}
	}

	public static abstract class Setting {

		public final String key;
		public final RPGPlayer player;

		public List<Entry<String, ItemStack>> items;
		protected int current;

		public Setting(RPGPlayer player, String key, int standard) {
			this.player = player;
			this.key = key;

			items = new ArrayList<>();

			putItems();

			String currentvalue = player.getSetting(key);
			if(currentvalue == null)
				current = standard;
			else {
				current = 0;

				for(Entry<String, ItemStack> e : items) {
					if(e.getKey().equalsIgnoreCase(currentvalue))
						break;
					else current++;
				}
				if(current >= items.size())
					current = standard;
			}
		}

		public void put(String key, ItemStack item) {
			items.add(new AbstractMap.SimpleEntry<>(key, item));
		}

		public void put(int slot, String key, ItemStack item) {
			items.add(slot, new AbstractMap.SimpleEntry<>(key, item));
		}

		public abstract void putItems();

		public ItemStack getAsItem() {
			
			return items.size() > 0 ? items.get(current).getValue() : null;
			
		}

		public ItemStack onToggle() {

			GameSound.play(Sound.ENTITY_ARROW_HIT_PLAYER, player.getPlayer(), 1f, 0.8f);

			String newSetting = (items.size() > ++current ? items.get(current) : items.get(current = 0)).getKey();
			player.setSetting(key, newSetting);

			return getAsItem();
		}

	}

	public static class ExpGainSetting extends Setting {

		public ExpGainSetting(RPGPlayer player) {
			super(player, "expMessage", 1);
		}

		@Override
		public void putItems() {
			put("GAIN", ItemUtils.create(Material.PAPER, "&aActionbar Exp Message", "Cummalative gains of recently earnt exp."));
			put("BAR", ItemUtils.create(Material.PAPER, "&aActionbar Exp Message", "Show a bar with progress to next level."));
			put("STATS", ItemUtils.create(Material.PAPER, "&aActionbar Exp Message", "Show your current and your missing exp amounts."));
			put("OFF", ItemUtils.create(Material.EMPTY_MAP, "&cOff", "Actionbar shows no exp messages."));
			addToggleMenu(items);
		}

	}

	public static class ScoreboardSetting extends Setting {

		public ScoreboardSetting(RPGPlayer player) {
			super(player, "scoreboard", 0);
		}

		@Override
		public void putItems() {
			put("LEVEL", ItemUtils.create(Material.PAPER, "&eScoreboard Setting", "Show the scoreboard for a time period after you level up."));
			put("ON", ItemUtils.create(Material.PAPER, "&eScoreboard Setting", "Leave the scoreboard always on."));
			put("NEVER", ItemUtils.create(Material.EMPTY_MAP, "&cOff", "Disable the scoreboard."));
			addToggleMenu(items);
		}

		@Override
		public ItemStack onToggle() {
			ItemStack result = super.onToggle();
			if(current != 0);
				player.updateScoreboard();
			return result;
		}

	}

	public static class StaminaSetting extends Setting {

		public StaminaSetting(RPGPlayer player) {
			super(player, "showStamina", 1);
		}

		@Override
		public void putItems() {
			put("TRUE", ItemUtils.create(Material.PAPER, "&bStamina &a"+ Symbol.YES, "Show Stamina exp."));
			put("FALSE", ItemUtils.create(Material.EMPTY_MAP, "&bStamina &c"+Symbol.NO, "Do not show Stamina exp."));
			addToggleMenu(items);
		}

	}

}
