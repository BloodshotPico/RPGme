package com.rpgme.plugin.integration.plugin;

import com.bekvon.bukkit.residence.protection.ClaimedResidence;
import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class ResidenceHook implements BlockProtectionPlugin {
	
	@Override
	public void enable(Plugin plugin) {
	
	}

	@Override
	public boolean canChange(Player p, Block block) {
		ClaimedResidence claim = getResidence(block);
		return claim == null || claim.getPermissions().playerHas(p.getName(), p.getWorld().getName(), "build", true);
	}
	
	@Override
	public boolean isInClaim(Block block) {
		return getResidence(block) != null;
	}
	
	private ClaimedResidence getResidence(Block block) {
		return com.bekvon.bukkit.residence.Residence.getResidenceManager().getByLoc(block.getLocation());
	}

	@Override
	public String getPluginName() {
		return "Residence";
	}

}
