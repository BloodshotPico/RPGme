package com.rpgme.plugin.integration.plugin;

import com.rpgme.plugin.integration.PlayerRelation;
import com.rpgme.plugin.integration.PluginIntegration.BlockProtectionPlugin;
import com.rpgme.plugin.integration.PluginIntegration.TeamPlugin;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;


public class FactionsUUIDHook implements BlockProtectionPlugin, TeamPlugin {
	
	@Override
	public void enable(Plugin plugin) {
	
	}

	@Override
	public boolean canChange(Player p, Block block) {
		return false;
	}

	@Override
	public boolean isInClaim(Block block) {
		return false;
	}

	@Override
	public PlayerRelation getRelation(Player one, Player two) {
		return PlayerRelation.NEUTRAL;
	}

	//
//	@Override
//	public PlayerRelation getRelation(Player one, Player two) {
//		FPlayer playerOne = FPlayers.getInstance().getByPlayer(one);
//		FPlayer playerTwo = FPlayers.getInstance().getByPlayer(two);
//		return translate(playerOne.getRelationTo(playerTwo));
//	}
//
//	private PlayerRelation translate(Relation rel) {
//		switch(rel) {
//		case MEMBER:
//		case ALLY: return PlayerRelation.TEAM;
//		case ENEMY: return PlayerRelation.ENEMIES;
//		case NEUTRAL:
//		case TRUCE:
//		default: return PlayerRelation.NEUTRAL;
//		}
//	}
//
//	@Override
//	public boolean canChange(Player player, Block block) {
//		return FactionsBlockListener.playerCanBuildDestroyBlock(player, block.getLocation(), "destroy", true);
//	}
//
//	@Override
//	public boolean isInClaim(Block block) {
//		FLocation location = new FLocation(block);
//		return Board.getInstance().getFactionAt(location) != null;
//	}

	@Override
	public String getPluginName() {
		return "Factions";
	}
	
	

}
