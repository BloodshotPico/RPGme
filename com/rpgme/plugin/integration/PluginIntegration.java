package com.rpgme.plugin.integration;

import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.integration.plugin.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PluginIntegration {

	private static final PluginIntegration i = new PluginIntegration();

	public static PluginIntegration getInstance() {
		return i;
	}

    /**
     * Base Interface to represent a hook into an other plugin
     * Note: do not implement this class directly. Instead implement one of the extended interfaces.
     */
    public interface PluginHook {

        void enable(Plugin plugin);
        String getPluginName();
    }

    /**
     * Plugin hook that hooks into a block-protection plugin.
     * These hooks are used to see if blocks may be changed by various abilities
     */
    public interface BlockProtectionPlugin extends PluginHook {

        boolean canChange(Player p, Block block);
        boolean isInClaim(Block block);
    }

    /**
     * Plugin hook that hooks into a plugin that defines player relations.
     * This is used to determine if two players are allies, neutral or enemies.
     */
    public interface TeamPlugin extends PluginHook {

        PlayerRelation getRelation(Player one, Player two);
    }

	private WGCustomFlagsHook wgFlags;
    private WorldGuardHook worldGuardHook;
	private List<BlockProtectionPlugin> protectionPlugins = new ArrayList<>();
	private TeamPlugin teamPlugin;

    private PluginIntegration() {
        PluginManager pm = Bukkit.getPluginManager();

        if(isPluginEnabled(pm, "Towny")) {
            register(new TownyHook());
        }
        if(isPluginEnabled(pm, "Residence")) {
            register(new ResidenceHook());
        }
        if(isPluginEnabled(pm, "GriefPrevention")) {
            register(new GriefPreventionHook());
        }
        if(isPluginEnabled(pm, "Kingdoms")) {
            register(new KingdomsHook());
        }
        if(isPluginEnabled(pm, "RedProtect")) {
            register(new RedProtectHook());
        }
        if(isPluginEnabled(pm, "PlaceHolderAPI")) {
            RPGmePlaceholderHook.register();
        }
        if(isPluginEnabled(pm, "WorldGuard")) {
            register(new WorldGuardHook());

            if(isPluginEnabled(pm, "WGCustomFlags")) {
                register(new WGCustomFlagsHook());
            }
        }

        Plugin factions = Bukkit.getPluginManager().getPlugin("Factions");
        if(factions != null) {
            String className = factions.getClass().getSimpleName();
            if(className.equals("Factions")) {
                register(new FactionsHook());
            }else if(className.equals("P")) {
                register(new FactionsUUIDHook());
            }
        }

        logResults();
    }

    private boolean isPluginEnabled(PluginManager manager, String pluginName) {
        return manager.getPlugin(pluginName) != null;
    }

    /**
     * Register a hook so that RPGme considers it in various checks.
     * Your class should either implement {@link TeamPlugin} or {@link BlockProtectionPlugin} for it to have any effect.
     * @return true if successfully hooked. False otherwise.
     */
    public boolean register(PluginHook hook) {
        if(hook == null) {
            throw new NullPointerException("PluginHook may not be null");
        }

        PluginManager pm = Bukkit.getPluginManager();
        Plugin plugin = pm.getPlugin(hook.getPluginName());

        if(plugin != null) {

            try {
                hook.enable(plugin);
            } catch (Exception e) {
                RPGme.getInstance().getLogger().log(Level.SEVERE, "Error while enabling hook for plugin " + hook.getPluginName(), e);
                return false;
            }

            if(hook instanceof BlockProtectionPlugin) {
                protectionPlugins.add((BlockProtectionPlugin) hook);
            }
            if(hook instanceof TeamPlugin) {
                if(teamPlugin == null) {
                    teamPlugin = (TeamPlugin) hook;
                }
                else {
                    RPGme.getInstance().getLogger().severe("Interfering Team plugins detected "
                            + "("+teamPlugin.getPluginName() + " and "+ hook.getPluginName()
                            + ")! Using only "+teamPlugin.getPluginName() + " to define Player relations.");
                    return false;
                }
            }

            if(hook instanceof WGCustomFlagsHook) {
                wgFlags = (WGCustomFlagsHook) hook;
            }

            if(hook instanceof WorldGuardHook) {
                worldGuardHook = (WorldGuardHook) hook;
            }
            return true;
        }
        return false;
    }

	private void logResults() {
        Logger log = RPGme.getInstance().getLogger();

		if(!protectionPlugins.isEmpty()) {
			StringBuilder builder = new StringBuilder();
			for(BlockProtectionPlugin pl : protectionPlugins) {
			    if(builder.length() > 0)
			        builder.append(", ");

				builder.append(pl.getPluginName());
			}

			if(builder.length() > 0) {
                builder.insert(0, "Hooked into block protection plugin(s): [");
                builder.append("]");

                log.info(builder.toString());
            }
		}

		if(teamPlugin != null) {
			log.info("Hooked into teams plugin: "+teamPlugin.getPluginName());
		}

        if(worldGuardHook != null) {
            if(wgFlags != null) {
                log.info("Hooked into WorldGuard with WorldGuardCustomFlags");
            }
            else {
                log.info("Hooked into WorldGuard");
            }
        }

        if(teamPlugin != null) {
            log.info("Hooked into teams plugin: "+teamPlugin.getPluginName());
        }
	}

	/*
	 * WorldGuardHook custom flags
	 */
	public double getExpMultiplier(Player player, Location location) {
		return wgFlags == null ? 1.0 : wgFlags.getExpValueAt(player, location);
	}

	public boolean isRPGmeEnabled(Player player, Location location) {
		return wgFlags == null || wgFlags.isPluginEnabledAt(player, location);
	}

	public boolean allowPvP(Player player, Location location) {
        return worldGuardHook == null || worldGuardHook.allowPvP(player, location);
    }

	/*
	 * Public methods that consult hooked plugins
	 */
	public boolean canChange(Player p, Block block) {
		if(protectionPlugins.isEmpty() || p == null || block == null)
			return true;

		for(BlockProtectionPlugin plugin : protectionPlugins) {

			try {
				if(!plugin.canChange(p, block))
					return false;
			} catch(Throwable e) {
				RPGme.getInstance().getLogger().severe("Error while consulting block protection manager "+plugin.getPluginName());
			}

		}

		return true;
	}

	public boolean isInClaim(Block block) {
		if(protectionPlugins.isEmpty() || block == null)
			return false;

		for(BlockProtectionPlugin plugin : protectionPlugins) {

			try {
				if(plugin.isInClaim(block))
					return true;
			} catch(Throwable e) {
				RPGme.getInstance().getLogger().severe("Error while consulting block protection manager "+plugin.getPluginName());
			}
		}

		return false;
	}

	public PlayerRelation getRelation(Player one, Player two) {
		if(one == two)
			return PlayerRelation.SELF;

		if(teamPlugin == null || one == null || two == null)
			return PlayerRelation.NEUTRAL;

		try {
			return teamPlugin.getRelation(one, two);
		} catch(Throwable e) {
			RPGme.getInstance().getLogger().severe("Error while consulting teams manager "+teamPlugin.getPluginName());
			return PlayerRelation.NEUTRAL;
		}
	}

}
