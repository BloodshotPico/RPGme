package com.rpgme.plugin.event;

import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SkillExpGainEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled = false;
	
	private final RPGPlayer player;
	private final Skill skill;
	private float exp;
	
	public SkillExpGainEvent(RPGPlayer p, Skill skill, float exp) {
		this.player = p;
		this.skill = skill;
		this.exp = exp;
	}
	
	/**
	 * @return the exp that is earned by this event
	 */
	public float getExp() {
		return exp;
	}
	
	/**
	 * @return the current exp of the player for this skill
	 */
	public float getCurrentExp() {
		return player.getExp(skill.getId());
	}

	/**
	 * @param exp the exp to give to the player as a result of this event
	 */
	public void setExp(float exp) {
		this.exp = exp;
	}

	/**
	 * @return the player
	 */
	public Player getPlayer() {
		return player.getPlayer();
	}
	
	/**
	 * @return the RPGPlayer
	 */
	public RPGPlayer getRPGPlayer() {
		return player;
	}

	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	public int getSkillId() {
		return skill.getId();
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean value) {
		cancelled = value;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}

}
