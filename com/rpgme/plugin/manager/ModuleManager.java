package com.rpgme.plugin.manager;

import com.google.common.collect.Iterables;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Module;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by Robin on 12/02/2017.
 */
public class ModuleManager <B extends Module> implements Module {

    private final RPGme plugin;
    private final IntMap<B> modules = new IntMap<>();
    private final IntMap<B> disabledModules = new IntMap<>();
    private final List<Integer> notDisableable = new ArrayList<>();

    public ModuleManager(RPGme plugin) {
        this.plugin = plugin;
    }

    public RPGme getPlugin() {
        return plugin;
    }

    public IntMap<B> getModules() {
        return modules;
    }

    public IntMap<B> getDisabledModules() {
        return disabledModules;
    }

    public boolean isEnabled(int id) {
        return modules.containsKey(id);
    }

    public boolean isEnabled(Module module) {
        return modules.containsValue(module, true);
    }

    public boolean containsName(String name) {
        return getByName(name) != null;
    }

    public void setCannotBeDisabled(int... ids) {
        for(int id : ids) {
            notDisableable.add(id);
        }
    }

    @SuppressWarnings("unchecked")
    public B getById(int id) throws ClassCastException {
        if(modules.containsKey(id))
            return modules.get(id);

        return disabledModules.get(id);
    }

    @SuppressWarnings("unchecked")
    public B getByName(String name) throws ClassCastException {
        for(B module : modules.values()) {
            if(module.getName().equalsIgnoreCase(name))
                return module;
        }
        for(B module : disabledModules.values()) {
            if(module.getName().equalsIgnoreCase(name))
                return module;
        }
        return null;
    }

    public boolean containsId(int id) {
        return modules.containsKey(id) || disabledModules.containsKey(id);
    }

    protected Iterable<IntMap.Entry<B>> allModules() {
        return Iterables.concat(modules.entries(), disabledModules.entries());
    }

    @Override
    public void createConfig(ConfigBuilder config, BundleBuilder messages) {
        for(IntMap.Entry<B> entry : allModules()) {
            B module = entry.value;

            String comment = StringUtils.isEmpty(module.getConfigDescription()) ? null : module.getConfigDescription();
            String name = module.getName();

            config.beginSection(comment, module.getName());
            messages.setPrefix(name);
            if(comment != null) {
                messages.addComment(comment);
            }

            if(!notDisableable.contains(entry.key)) {
                config.addValue("enabled", true);
            }

            int bundleSize = messages.size();
            try {
                module.createConfig(config, messages);
            } catch (Exception e) {
                logError("Module " + module.getName() + " failed to create configuration", e);
                disabledModules.put(entry.key, module);
            }

            config.endSection();
            if(messages.size() > bundleSize) {
                messages.addNewline();
            }
            messages.setPrefix(null);
        }
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        for(IntMap.Entry<B> entry : allModules()) {

            B module = entry.value;
            String name = module.getName();

            ConfigurationSection configSection = config.getConfigurationSection(name);
            BundleSection bundleSection = messages.getSection(name);

            if(configSection == null)
                // section may not exist if the module is declared as not disableable and did not add any options in createConfig()
                configSection = config.createSection(name);

            if(!configSection.getBoolean("enabled", true)) {
                disabledModules.put(entry.key, module);
                continue;
            }

            try {
                module.onLoad(configSection, bundleSection);
            } catch (Exception e) {
                logError("Module " + module.getName() + " failed to load configuration", e);
                disabledModules.put(entry.key, module);
            }
        }

        // If not enabled, disable now.
        // But keep reference in separate map for the next configuration pass.
        IntMap.Keys disabledIds = disabledModules.keys();
        while(disabledIds.hasNext) {
            removeModule(disabledIds.next());
        }
    }

    @Override
    public void onDisable() {
        for(Module module : modules.values()) {
            try {
                module.onDisable();
            } catch (Exception e) {
                logError("Exception while disabling module " + module.getName(), e);
            }
        }
    }

    /**
     * Register a new module in this manager. A registered module will:
     * <ul>
     *     <li>Be registered as a Listener</li>
     *     <li>Have it's lifecycle methods called</li>
     *     <li>Be part of the configuration pass</li>
     *     <li>Have an generated 'enabled' configuration option, which is handled by this manager.</li>
     * </ul>
     * @param id
     * @param module
     * @return true if registration was successful
     *
     * @throws IllegalArgumentException if the provided id is already used by an other module
     * @throws IllegalArgumentException if modules getName() method is null, empty or the same as an already registered module
     */
    public boolean registerModule(int id, B module) {
        if(module == null)
            return false;

        if(containsId(id)) {
            throw new IllegalArgumentException("Id " + id + " is already used to register " + modules.get(id).getClass()
                    + ". Remove that first if you wish to replace it.");
        }

        if(StringUtils.isEmpty(module.getName())) {
            throw new IllegalArgumentException("Module " + module.getClass() + " has a null or empty name");
        }

        if(containsName(module.getName())) {
            throw new IllegalArgumentException("Name " + module.getName() + " is already taken by " + modules.get(id).getClass()
                    + ". Remove that first if you wish to replace it.");
        }

        try {
            module.onEnable();
        } catch(Exception e) {
            logError("Exception while enabling module " + module.getName(), e);
            return false;
        }

        if(module instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) module, plugin);
        }

        modules.put(id, module);
        return true;
    }

    /**
     * Unregister a Module
     * This will remove it from this manager, unregister it's EventHandlers and call it's onDisable method.
     * @param id id the module was previously registered with
     * @return The Module instance that was removed, or null if there was no module for the given id.
     */
    public B removeModule(int id) {
        B module = modules.remove(id);

        if(module != null) {

            if (module instanceof Listener) {
                HandlerList.unregisterAll((Listener) module);
            }
            try {
                module.onDisable();
            } catch (Exception e) {
                logError("Exception while disabling module " + module.getName(), e);
            }
        }
        return module;
    }

    protected void logError(String error, Throwable exception) {
        plugin.getLogger().log(Level.SEVERE, error, exception);
    }

    protected void logError(String error) {
        plugin.getLogger().log(Level.SEVERE, error);
    }

}
