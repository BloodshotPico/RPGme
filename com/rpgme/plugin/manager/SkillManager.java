package com.rpgme.plugin.manager;

import com.google.common.collect.Lists;
import com.rpgme.plugin.RPGme;
import com.rpgme.plugin.api.Skill;
import com.rpgme.plugin.player.RPGPlayer;
import com.rpgme.plugin.util.IntMap;
import com.rpgme.plugin.util.TimeUtils;
import com.rpgme.plugin.util.config.BundleBuilder;
import com.rpgme.plugin.util.config.BundleSection;
import com.rpgme.plugin.util.config.ConfigBuilder;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class SkillManager extends ModuleManager<Skill> {

    private Configuration skillConfig;

    private String colorPrimary, colorPrimaryDark, colorAccent;
    private int targetLevel;

    public SkillManager(RPGme plugin) {
        super(plugin);
    }

    @Override
    public void createConfig(ConfigBuilder skillConfig, BundleBuilder messages) {
        // settings for the main config
        messages.addHeader("Color Theme")
                .addValue("Set bukkit colors to theme various ui elements", "color_primary", "&e")
                .addValue("color_primaryDark", "&6").addValue("color_accent", "&b")

                .addNewline().addHeader("Exp messages")
                .addValue("message when you level up. Placeholder 0 gets replaced with the skill, and 1 with the level", "exp_levelup", "&aYour {0} has increased to &2level &l{1}")
                .addValue("Format for the gains-style message", "exp_gain", "&6+{0} &eexp &f(&7{1}&f)")
                .addValue("Format for the stats-style message", "exp_stats", "&e{0} &7{1} &8| {2}")
                .addValue("Format for the bar-style message", "exp_bar", "&6[||||||||||||||||||||||||||||||&6] &8- &e{0}")
                .addValue("character(s) in the above string that getEntry colored", "exp_bar_char", "|")

                .addNewline().addHeader("UI")

                .addValue("cooldown message, with the placeholder as time", "ui_err_oncooldown", "&9On Cooldown for &b{0}")
                .addValue("ui_err_needhigherlevel", "&cYour {1} needs to be atleast level &f{0} &cto do that.")
                .addValue("ui_notification_title_format", "&6{0} &2: &9&l{1} &7- {2}")

                .addValue("skills_total", "Total:")
                .addValue("skills_combat", "Combat Level")
                .addValue("skills_hoverme", "Tip: Use your mouse")
                .addValue("ui_url", "&eClick for external information")
                .addValue("ui_nextlevel", "Until next:")
                .addValue("ui_currentlevel", "Level:")
                .addValue("ui_next", "&7&oNext: &l{0} &7&oat level &f&o{1}")
                .addValue("ui_stats", "&6Stats:")
                .addValue("ui_messages", "&aMessages:")
                .addValue("ui_clickme", "Click for skill info")
                .addValue("ui_settings_title", "&cRPGme Settings")
                .addValue("ui_settings", "Settings")
                .addValue("ui_settings_clickme", "Click to open")
                .addValue("ui_scoreboard_title", "&lRPGme")
                .addNewline()
                ;

        skillConfig.addLargeHeader("RPGme Skill configuration").addNewline();

        super.createConfig(skillConfig, messages);
    }

    @Override
    public void onLoad(ConfigurationSection config, BundleSection messages) {
        skillConfig = config.getRoot();

        colorPrimary = messages.getMessage("color_primary");
        colorPrimaryDark = messages.getMessage("color_primaryDark");
        colorAccent = messages.getMessage("color_accent");

        super.onLoad(skillConfig, messages);
    }

    public int getTargetLevel() {
        if(targetLevel == 0) {
            FileConfiguration globalConfig = RPGme.getInstance().getConfig();
            targetLevel = globalConfig.getInt("PlayerManager.Target Level");
        }
        return targetLevel;
    }

    public Configuration getSkillConfig() {
        return skillConfig;
    }

    public String colorDark() {
        return colorPrimaryDark;
    }

    public String colorLight() {
        return colorPrimary;
    }

    public String colorAccent() {
        return colorAccent;
    }

    @Override
    public Skill getByName(String name) throws ClassCastException {
        Skill namedSkill = super.getByName(name);
        if(namedSkill != null)
            return namedSkill;

        for(IntMap.Entry<Skill> entry : allModules()) {
            Skill skill = entry.value;

            if(skill.hasDisplayName() && skill.getDisplayName().equalsIgnoreCase(name)) {
                return skill;
            }
        }
        return null;
    }

    public int getSkillCount() {
        return getModules().size();
    }

    public List<Skill> getEnabledSkills() {
        return Lists.newArrayList(getModules().values().iterator());
    }

    public List<Skill> getEnabledSkills(Player p) {
        RPGPlayer rp = getPlugin().getPlayer(p);
        if(rp == null) {
            return Collections.emptyList();
        }
        return getEnabledSkills(rp);
    }

    public List<Skill> getEnabledSkills(RPGPlayer rp) {
        List<Skill> list = new ArrayList<>(getSkillCount());
        for(Skill skill : getEnabledSkills()) {
            if(skill.isEnabled(rp)) {
                list.add(skill);
            }
        }
        return list;
    }

    public List<Integer> getKeys() {
        List<Integer> list = new ArrayList<>(getModules().size());
        getModules().entries().forEach((entry) -> list.add(entry.key));
        return list;
    }

    public String buildCooldownMessage(long millis) {
        String time = TimeUtils.readableAsMinuteFormat((int) (millis/1000));
        return getPlugin().getMessages().getMessage("ui_err_oncooldown", time);
    }

}
